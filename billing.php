<?php
// header
$bodyclass = 'account';
require_once('_header.php');
?>
<!-- header w/ tabs -->
<div id="header" class="container-max">
    <h2>Account Settings</h2>
    <div class="clearfix"></div>
    <ul class="nav nav-tabs">
        <li><a href="account.php">Advertiser Profile</a></li>
        <li class="active"><a href="billing.php">Billing Panel</a></li>
    </ul>
</div>

<div class="container" id="maincontent">
    <p class="no-margin-top alert alert-success" role="alert"><i class="fa icon fa-check"></i> Your deposit is being processed and should be displayed below in the next few minutes.</p>


    <div class="col-md-6 no-padding">
        <table class="table borderless no-margin"><tr>
        <td class="text-left">
            <p class="gray"><span class="hidden-xs">Current </span>Balance:</p>
            <h1 class="no-margin green">$728<small class="green">.12</small></h1>
        </td>
        <td class="text-left hidden-xs">
            <p class="gray">Last Deposit:</p>
            <h1 class="no-margin">$1,600<small>.00</small></h1>
        </td>
        <td class="text-left">
            <p class="gray"><span class="hidden-xs">Current </span>Budget:</p>
            <h1 class="no-margin">$340<small>.00 /day</small></h1>
        </td>
        </tr></table>
    </div>

    <div class="col-md-6 no-padding" id="paybuttons">
        <p class="no-margin">Deposit additional funds using</p>
        <a href="#" class="btn btn-green btn-lg" data-toggle="modal" data-target="#ppModal"><i class="fa fa-icon fa-paypal"></i>&nbsp; PayPall&nbsp; </a>
        <a href="#" class="btn btn-silver btn-lg"><i class="fa fa-icon fa-credit-card"></i>&nbsp; Credit Card&nbsp; </a>
    </div>
    <div class="clearfix"></div>

    <hr>
    <h4 class="green simple" style="margin-bottom:10px">Transaction History</h4>
    <table class="table table-bordered table-hover" id="listitems">
        <thead>
            <tr class="active">
                <th class="text-left">Date</th>
                <th class="text-left">Status</th>
                <th class="hidden-xs text-left">Method</th>
                <th class="text-left">Txn Type</th>
                <th class="text-left">Txn ID</th>
                <th>Amount</th>
                <th class="hidden-xs" style="width:110px">Invoice</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-left">14-Nov-2014</td>
                <td class="text-left orange">PENDING</td>
                <td class="hidden-xs text-left"><i class="fa fa-icon fa-paypal"></i> PayPal</td>
                <td class="text-left">Deposit</td>
                <td class="text-left">EX1wBOMQ7GkDOLn0</td>
                <td>$1,600.00</td>
                <td class="hidden-xs"><a href="#" class="btn btn-silver btn-xs" data-toggle="modal" data-target="#invoiceModal"><i class="fa fa-icon fa-download"></i>&nbsp; Invoice&nbsp;</a></td>
            </tr>
            <tr>
                <td class="text-left red">12-Nov-2014</td>
                <td class="text-left red">DECLINED</td>
                <td class="hidden-xs text-left red"><i class="fa fa-icon fa-credit-card"></i> Credit Card</td>
                <td class="text-left">Deposit</td>
                <td class="text-left red">HL1lKMoPhd0U9jpm</td>
                <td class="red">$2,000.00</td>
                <td class="hidden-xs"><a href="#" class="btn btn-silver btn-xs" data-toggle="modal" data-target="#invoiceModal"><i class="fa fa-icon fa-download"></i>&nbsp; Invoice&nbsp;</a></td>
            </tr>
            <tr>
                <td class="text-left">12-Oct-2014</td>
                <td class="text-left green">Credited</td>
                <td class="hidden-xs text-left"><i class="fa fa-icon fa-institution"></i> Bank Wire</td>
                <td class="text-left">Adjustment</td>
                <td class="text-left">0MOG3qosiymNC4UJ</td>
                <td>$2,000.00</td>
                <td class="hidden-xs"><a href="#" class="btn btn-silver btn-xs" data-toggle="modal" data-target="#invoiceModal"><i class="fa fa-icon fa-download"></i>&nbsp; Invoice&nbsp;</a></td>
            </tr>
            <tr>
                <td class="text-left">12-Sep-2014</td>
                <td class="text-left green">Credited</td>
                <td class="hidden-xs text-left"><i class="fa fa-icon fa-money"></i> Manual</td>
                <td class="text-left">Bonus</td>
                <td class="text-left">0tfik2BC0tkrHBxT</td>
                <td>$2,000.00</td>
                <td class="hidden-xs"><a href="#" class="btn btn-silver btn-xs" data-toggle="modal" data-target="#invoiceModal"><i class="fa fa-icon fa-download"></i>&nbsp; Invoice&nbsp;</a></td>
            </tr>
            <tr>
                <td class="text-left">12-Sep-2014</td>
                <td class="text-left green">Credited</td>
                <td class="hidden-xs text-left"><i class="fa fa-icon fa-bitcoin"></i> Bitcoin</td>
                <td class="text-left">Withdrawal</td>
                <td class="text-left">com1lI1Ok0thUv4b</td>
                <td>$2,000.00</td>
                <td class="hidden-xs"><a href="#" class="btn btn-silver btn-xs" data-toggle="modal" data-target="#invoiceModal"><i class="fa fa-icon fa-download"></i>&nbsp; Invoice&nbsp;</a></td>
            </tr>
        </tbody>
    </table>

    <div class="clearfix"></div>

</form>

<?php
require_once('modal-invoice.php');
require_once('modal-paypal.php');
require_once('_pagination.php');
require_once('_footer.php');
?>