<?php
// header
$bodyclass = 'legal';
require_once('_header.php');
?>
<div id="header" class="container-max" style="margin-top:-6px"></div>

<div class="col-md-1" id="legal-list">
    <h4>See more</h4>
    <ul class="list-unstyled">
        <li><a href="./legal.php?title=advertising+guidelines">Advertising Guidelines</a></li>
        <li><a href="./legal.php?title=terms+of+service">Terms of Service</a></li>
        <li class="active"><a href="./legal.php?title=privacy+policy">Privacy Policy</a></li>
    </ul>
</div>
<div class="col-md-8" id="legal-main">
    <h1 style="text-transform:capitalize;"><?php echo (isset($_GET['title'])) ? urldecode(@$_GET['title']) : 'Page Title'; ?></h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc non elit vitae urna cursus pellentesque ac quis mi. Maecenas facilisis risus vel nibh consequat, ac pellentesque ipsum hendrerit. Aenean eget diam quis lectus euismod ultrices. Etiam at velit laoreet, vulputate lorem sollicitudin, fermentum turpis. Fusce cursus, turpis eu consectetur tincidunt, lorem urna auctor erat, blandit euismod orci sapien a odio. Nam maximus gravida imperdiet. Nulla condimentum sodales arcu in fermentum. Duis a libero sit amet mi convallis commodo vitae vel mauris. Proin laoreet fermentum metus quis consectetur.</p>
    <p>Nam laoreet et risus efficitur feugiat. Pellentesque venenatis lorem vel metus faucibus rutrum. Phasellus feugiat mattis pharetra. Morbi posuere enim sed nunc finibus malesuada. Phasellus finibus sit amet magna eget hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam eleifend est id justo rhoncus efficitur. Phasellus id nisl neque. Maecenas sodales erat sed ligula pulvinar, sit amet maximus ipsum elementum.</p>
    <p>Suspendisse blandit eros sed sapien fermentum, ut hendrerit sem malesuada. Mauris placerat aliquet arcu, vitae gravida metus sodales quis. In faucibus augue quis ultrices bibendum. Vestibulum ac accumsan ex. Nulla feugiat ligula non tortor elementum luctus. Proin blandit maximus lacus ac tempus. Donec aliquet posuere ligula, vitae imperdiet diam ullamcorper eget. Fusce viverra convallis lorem. Aliquam erat volutpat. Fusce vel massa turpis.</p>
    <p>Vestibulum lorem lectus, faucibus non lacus quis, scelerisque scelerisque felis. Nullam elit nibh, aliquam id commodo eget, molestie ut mauris. Duis eu eros erat. Nulla ac pulvinar ex. Mauris vitae augue ac lacus lacinia ultricies. Maecenas vel lobortis ante. Sed sit amet aliquet elit. Donec et tempor ipsum. Sed a malesuada est. Phasellus mi tellus, vulputate quis pellentesque euismod, viverra eget nibh. Etiam quis ornare metus. Cras efficitur purus massa, eget varius nisi aliquam in. Praesent convallis elit odio.</p>
    <p>Phasellus dictum efficitur dignissim. Aenean pretium sodales lacus, id ultrices lectus luctus vitae. Phasellus maximus rhoncus iaculis. Morbi volutpat justo auctor sodales interdum. Curabitur non elit varius, eleifend tortor ac, porta tortor. Praesent sagittis nisl orci, nec ultrices justo pellentesque sit amet. Etiam sagittis, eros quis gravida dapibus, libero metus dignissim erat, aliquet pharetra lectus nisl sit amet lacus. Sed purus mauris, tincidunt id eros in, tempus congue tellus. Curabitur commodo arcu purus, vel porttitor est tincidunt nec.</p>
    <p>Sed a turpis sed eros bibendum vulputate. Suspendisse potenti. Suspendisse nibh sem, ornare a tellus nec, vehicula porta sapien. Quisque dapibus lacus sit amet est sagittis tristique rhoncus id nunc. Nulla vel mauris libero. Aliquam leo sem, dapibus ut erat sed, ultricies placerat ex. Duis eget turpis sit amet dui maximus pellentesque in sit amet enim. Morbi at ante in elit hendrerit tincidunt. Aliquam est eros, molestie eu feugiat sed, commodo ac diam. Aliquam vehicula sit amet tortor ac scelerisque. Phasellus non interdum lectus, ac interdum dolor. Aliquam pulvinar libero vel lectus condimentum, et ultrices mauris consequat. Ut sit amet leo lorem. Praesent finibus non ligula eu placerat. Cras vulputate, nisi non tristique elementum, arcu nisl auctor arcu, in congue nibh velit et sem. Aenean egestas mattis odio, a efficitur leo aliquam consectetur.</p>
    <p>Donec ultrices finibus dolor, a placerat massa lacinia eget. Curabitur turpis augue, sollicitudin sed diam eu, vestibulum finibus neque. Aenean at ante non sapien fringilla rutrum eu quis purus. Ut volutpat egestas porttitor. Pellentesque non lorem semper lacus congue viverra. Nullam dictum tincidunt nibh placerat mattis. Pellentesque porta tincidunt nulla a maximus. Sed gravida eros a nisl lobortis ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas et cursus ipsum. Etiam et finibus felis.</p>
    <p>Praesent bibendum elit eu sem blandit laoreet. Praesent laoreet pharetra placerat. Morbi convallis arcu non nisi lobortis iaculis. Integer ac sodales ipsum, porttitor dictum nisi. Vivamus iaculis nunc id feugiat rutrum. Nam non est eget dui porttitor pretium. Sed nec tortor porta, venenatis nisi non, tempor erat. Ut eu risus porttitor ligula placerat dignissim. Phasellus ac risus sit amet felis posuere lobortis. Nullam scelerisque dolor leo, vitae fringilla orci feugiat non. Morbi lacinia ac orci ac sollicitudin.</p>

</div>

<div class="clearfix"></div>
<?php
require_once('_footer.php');
?>