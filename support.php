<?php
// header
$bodyclass = 'support';
require_once('_header.php');
?>

<!-- header w/ tabs -->
<div id="header" class="container-max">
    <h2>Help Center</h2>
    <div class="clearfix"></div>
    <ul class="nav nav-tabs">
        <li class="active"><a href="support.php">Knowledge Base</a></li>
        <li><a href="support-form.php">Support Desk</a></li>
    </ul>
</div>

<div class="container" id="maincontent">

    <h3 class="no-margin-top green simple">Frequently Asked Questions</h3>
    <p>Below is a list of the most frequently asked questions, along with theie answers.</p>
    <hr>

    <div id="faq" class="col-md-7">&nbsp;

        <p class="faq"><i class="green fa fa-question-circle"></i> Some question goes here</p>
        <p class="faa">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

        <p class="faq"><i class="green fa fa-question-circle"></i> Another question goes here</p>
        <p class="faa">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

        <p class="faq"><i class="green fa fa-question-circle"></i> The third question goes here</p>
        <p class="faa">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

        <p class="faq"><i class="green fa fa-question-circle"></i> One more question goes here</p>
        <p class="faa">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

    </div>


    <div class="pull-right col-md-4">
        <h4 class="simple">Still not getting it?!</h4>
        <p>Maybe you're stupid... Don't worry - we can help :) Just <a href="support-form.php">contact our support team</a> and we'll try to get through to you.</p>
    </div>
    <div class="clearfix"></div>


    <div class="clearfix"></div>
    <p>&nbsp;</p>

</form>

<?php
require_once('_footer.php');
?>