<?php
// header
$bodyclass = 'form campaign-create';
require_once('_header.php');

// sidebar
// require_once('_sidebar.php');

// top tabs
$active_tab = 'edit';
require_once('_tabs.php');
?>

<div class="container" id="maincontent">

<h3 class="no-margin-top green">Edit PPC Campaign</h3>
<hr>

<form class="form-horizontal" role="form">
    <!-- info -->
    <h4 class="col-sm-2 text-right no-margin-top green simple">Campaign Information</h4>
    <div class="clearfix"></div>

    <div class="form-group">
        <label for="inputName" class="col-sm-2 control-label">Group name:</label>
        <div id="group-new" style="display:none">
            <div class="col-sm-3"><input type="text" class="form-control" id="inputGroup" placeholder="Group name"></div>
            <div class="col-sm-2 light" style="padding-top:7px"><i class="fa fa-list-ul"></i> &nbsp;<a href="#" class="light" onclick="$('#group-new').toggle(); $('#group-list').toggle(); return false;">Show list</a></div>
        </div>
        <div id="group-list">
            <div class="col-sm-3"><select id="inputGroupList" class="form-control selectpicker" data-live-search="true">
                <option selected>Campaign Group Name</option>
                <option>Campaign Group 2</option>
                <option>...</option>
            </select></div>
            <div class="col-sm-2 light" style="padding-top:7px"><i class="fa fa-plus-square"></i> &nbsp;<a href="#" class="light" onclick="$('#group-new').toggle(); $('#group-list').toggle(); $('#inputGroup').focus(); return false;">Create new</a></div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Targeting Type:</label>
        <div class="col-sm-3"><select class="form-control selectpicker" onchange="if($(this).val() === 'kw') { $('#keywords-targeted').slideDown(); } else { $('#keywords-targeted').slideUp(); }">
            <option value="ron">Run-of-Network (more traffic)</option>
            <option value="kw" selected>Keyword-Targeted</option>
        </select></div>
        <div class="col-sm-1"><a href="#" onclick="$(this).parent().parent().next().slideToggle(); return false" class="help-icon"><i class="fa fa-question-circle"></i></a></div>
    </div>
    <div class="below-help" style="display:none">
        <div class="col-sm-offset-2 col-sm-5 help">With a Run-of-Network (RoN) campaign you will likely get more traffic to your page, whereas with a Keyword-Targeted campaign you can be more precise.</div>
        <div class="clearfix"></div>
    </div>

    <div class="col-sm-offset-2 col-sm-5"><hr style="margin:20px 0 10px 0"></div>
    <div class="clearfix"></div>

    <div class="form-group">
        <label for="inputName" class="col-sm-2 control-label">Campaign name:</label>
        <div class="col-sm-3"><input type="email" class="form-control" id="inputName" placeholder="Campaign name" value="Active Campaign"></div>
    </div>

    <div class="form-group">
        <label for="inputBudget" class="col-sm-2 control-label">Daily Budget:</label>
        <div class="col-sm-2 price-wrapper"><input class="pull-left form-control price" id="inputBudget" value="25.00"></div>
        <div class="col-sm-1"><a href="#" onclick="$(this).parent().parent().next().slideToggle(); return false" class="help-icon"><i class="fa fa-question-circle"></i></a></div>
    </div>
    <div class="below-help" style="display:none">
        <div class="col-sm-offset-2 col-sm-5 help">Your daily budget is the average amount you're comfortable spending each day on this campaign.
            <span class="ppc-item"><br> As traffic fluctuates, you may spend up to 20% more than your daily budget on a given day.</span>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="form-group">
        <label for="inputCPC" class="col-sm-2 control-label">CPC:</label>
        <div class="col-sm-2 price-wrapper"><input class="pull-left form-control price" id="inputCPC" value="0.01"></div>
    </div>

    <div class="col-sm-offset-2 col-sm-5"><hr style="margin:20px 0 10px 0"></div>
    <div class="clearfix"></div>

    <!-- ad -->
    <div class="clearfix" style="height:30px"></div>
    <h4 class="col-sm-2 text-right no-margin-top green simple">Campaign Ad</h4>
    <div class="clearfix"></div>

    <div id="ppcpreview" class="col-sm-offset-6 col-sm-3 hidden-sm hidden-xs">
        <strong>Preview:</strong>
        <div id="ppcpreview_wrapper">
            <div id="ppcpreview_title">Running Shoes‎</div>
            <div id="ppcpreview_text1">Free Ship &amp; Perfect Fit Guarantee</div>
            <div id="ppcpreview_text2">On All Of Your Favorite Shoes!</div>
            <div id="ppcpreview_dispurl">www.roadrunnersports.com</div>
        </div>
    </div>
    <div class="form-group">
        <label for="inputTitle" class="col-sm-2 control-label">Headline:</label>
        <div class="col-sm-3"><input type="text" class="form-control" id="inputTitle" value="Running Shoes‎" placeholder="Headline" onkeyup="$('#ppcpreview_title').html((this.value=='')?this.placeholder:this.value);"></div>
    </div>
    <div class="form-group">
        <label for="inputText1" class="col-sm-2 control-label">Description:</label>
        <div class="col-sm-4"><input type="text" class="form-control" id="inputText1" value="Free Ship &amp; Perfect Fit Guarantee" placeholder="Description line 1" onkeyup="$('#ppcpreview_text1').html((this.value=='')?this.placeholder:this.value);"></div>
    </div>
    <div class="form-group" style="margin-top:5px !important">
        <label for="inputText2" class="col-sm-2 control-label"></label>
        <div class="col-sm-4"><input type="text" class="form-control" id="inputText2" value="On All Of Your Favorite Shoes!" placeholder="Description line 2" onkeyup="$('#ppcpreview_text2').html((this.value=='')?this.placeholder:this.value);"></div>
    </div>
    <div class="form-group">
        <label for="inputDispURL" class="col-sm-2 control-label">Display URL:</label>
        <div class="col-sm-3"><input type="text" class="form-control" id="inputDispURL" value="www.roadrunnersports.com" placeholder="Display URL" onkeyup="$('#ppcpreview_dispurl').html((this.value=='')?this.placeholder:this.value);"></div>
    </div>
    <div class="form-group">
        <label for="inputUrl" class="col-sm-2 control-label">Destination URL:</label>
        <div class="col-sm-5"><input type="text" class="form-control" id="inputUrl" value="http://www.roadrunnersports.com/?source=%%source%%&kw=%%keyword%%" placeholder="http://"></div>
        <div class="col-sm-1"><a href="#" onclick="$(this).parent().parent().next().slideToggle(); return false" class="help-icon"><i class="fa fa-question-circle"></i></a></div>
    </div>
    <div class="below-help" style="display:none">
        <div class="col-sm-offset-2 col-sm-5 help">You can use <code>%%source%%</code> or <code>%%keyword%%</code> in your URLs and we'll pass the Source ID and Keyword to your destination URL for easy tracking.</div>
        <div class="clearfix"></div>
    </div>

    <div class="col-sm-offset-2 col-sm-5"><hr style="margin:20px 0 10px 0"></div>
    <div class="clearfix"></div>


    <!-- targeting -->
    <div class="clearfix" style="height:40px"></div>
    <h4 class="col-sm-2 text-right no-margin-top green simple">Campaign Targeting</h4>
    <div class="clearfix"></div>

    <!-- keywords targeting -->
    <div class="form-group form-group-tight" id="keywords-targeted">
        <label class="col-sm-2 control-label">Keywords:</label>
        <div class="col-md-3">
            <h5><i class="fa fa-check green"></i> Active Keywords</h5>
            <textarea name="keywords" class="form-control" rows="15" placeholder="Enter the keywords you want to target">shopping
amazon.com/*/shopping</textarea>
        </div>
        <div class="col-md-2 no-padding-left">
            <h5><i class="fa fa-ban red"></i> Blocked Keywords</h5>
            <textarea name="keywords-blocked" class="form-control" rows="15" placeholder="Enter keywords to block">cheap
coupons</textarea>
        </div>
        <div class="clearfix"></div>
        &nbsp;
    </div>

    <!-- source targeting -->
    <div class="form-group form-group-tight" id="source-targeted">
        <label class="col-sm-2 control-label">Sources:</label>
        <div class="col-md-3 nso-padding-left">
            <h5><i class="fa fa-ban red"></i> Blocked Source IDs</h5>
            <textarea name="source-blocked" class="form-control" rows="10" placeholder="Enter source id to block">17252
62802
32984
81231</textarea>
        </div>
    </div>


    <!-- campaign targeting -->
    <div class="clearfix" style="height:40px"></div>
    <h4 class="col-sm-2 text-right no-margin-top green simple">Audience Targeting</h4>
    <div class="clearfix"></div>
    <div class="form-group form-group-tight">
        <label for="inputGeo" class="col-sm-2 control-label">Countries:</label>
        <div class="col-sm-2 no-padding-left"><a href="#" onclick="$(this).parent().parent().next().slideToggle(); return false" class="btn" style="margin-top:1px"><span id="geoIndicator">All Countries</span> <i class="fa fa-pencil"></i></a></div>
    </div>
    <div class="form-groups below-help" style="display:none">
        <div class="col-sm-offset-2 col-sm-8 expanded">
            <ul class="list-inline list-unstyled" id="geo-checkboxes-groups">
                <li><input type="checkbox" name="geoselect" checked onclick="geoSelect()" value="tier1" id="geo-group-tier1"> <label for="geo-group-tier1" class="simple">Tier 1 Countries</label></li>
                <li><input type="checkbox" name="geoselect" checked onclick="geoSelect();" value="tier2" id="geo-group-tier2"> <label for="geo-group-tier2" class="simple">Tier 2 Countries</label></li>
                <li><input type="checkbox" name="geoselect" checked onclick="geoSelect();" value="tier3" id="geo-group-tier3"> <label for="geo-group-tier3" class="simple">Tier 3 Countries</label></li>
                <li style="border-left:1px solid #ccc;margin-left:10px;padding-left:10px"><a href="#" onclick="geoSelect('all'); return false"> Select/Deselect all</a></li>
            </ul>
            <hr>
            <ul class="list-unstyled list-quad" id="geo-checkboxes"><?php
            include('_countries.php');
            foreach ($FLATDB_GEOS_CODES as $code=>$name){
                echo '<li><input type="checkbox" name="countries[]" checked value="'.$code.'" onchange="geoSingleChange($(this))" id="geo-'.$code.'"><label for="geo-'.$code.'" class="simple">'.$name.'</label></li>';
            }
            ?><div class="clearfix"></div></ul>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="col-sm-offset-2 col-sm-5"><hr style="margin:10px 0 10px 0"></div>
    <div class="clearfix"></div>

    <!-- devices -->
    <div class="form-group form-group-tight">
        <label for="inputDevices" class="col-sm-2 control-label">Devices:</label>
        <div class="col-sm-8" style="padding-top:8px">
            <ul class="list-inline list-unstyled">
                <li><input type="checkbox" name="devices[]" checked value="DesktopsLaptops" id="inputDesktops"> <label for="inputDesktops" class="simple">Desktops &amp; Laptops</label></li>
                <li><input type="checkbox" name="devices[]" checked value="Tablets" id="inputTablets"> <label for="inputTablets" class="simple">Tablets</label></li>
                <li><input type="checkbox" name="devices[]" checked value="Phones" id="inputPhones"> <label for="inputPhones" class="simple">Smartphones</label></li>
            </ul>
        </div>
    </div>
    <!-- platforms -->
    <div class="form-group form-group-tight">
        <label for="inputDevices" class="col-sm-2 control-label">Platforms:</label>
        <div class="col-sm-8" style="padding-top:8px">
            <ul class="list-inline list-unstyled">
                <li><input type="checkbox" name="platforms[]" checked value="windows" id="inputTabletWindows"> <label for="inputTabletWindows" class="simple">Windows</li>
                <li><input type="checkbox" name="platforms[]" checked value="macos" id="inputMacOS"> <label for="inputMacOS" class="simple">OS-X (Macintosh)</label></li>
                <li><input type="checkbox" name="platforms[]" checked value="linux" id="inputLinux"> <label for="inputLinux" class="simple">Linux</label></li>
                <li><input type="checkbox" name="platforms[]" checked value="android" id="inputTabletAndroid"> <label for="inputTabletAndroid" class="simple">Android</label></li>
                <li><input type="checkbox" name="platforms[]" checked value="ios" id="inputTabletIOS"> <label for="inputTabletIOS" class="simple">iOS (iPhones/iPads/iPods)</li>
            </ul>
        </div>
    </div>

    <!-- browsers -->
    <div class="form-group form-group-tight">
        <label for="inputDevices" class="col-sm-2 control-label">Browsers:</label>
        <div class="col-sm-8" style="padding-top:8px">
            <ul class="list-inline list-unstyled">
                <li><input type="checkbox" name="desktop_clients[]" checked value="chrome" id="inputChrome"> <label for="inputChrome" class="simple">Chrome</label></li>
                <li><input type="checkbox" name="desktop_clients[]" checked value="msie" id="inputMSIE"> <label for="inputMSIE" class="simple">Internet Explorer</label></li>
                <li><input type="checkbox" name="desktop_clients[]" checked value="firefox" id="inputFirefox"> <label for="inputFirefox" class="simple">Firefox</label></li>
                <li><input type="checkbox" name="desktop_clients[]" checked value="safari" id="inputSafari"> <label for="inputSafari" class="simple">Safari</label></li>
                <li><input type="checkbox" name="desktop_clients[]" checked value="opera" id="inputOther"> <label for="inputOther" class="simple">Other</label></li>
            </ul>
        </div>
    </div>

    <!-- capping -->
    <div class="clearfix" style="height:30px"></div>
    <h4 class="col-sm-2 text-right no-margin-top green simple">Campaign Delivery</h4>
    <div class="clearfix"></div>
    <div class="form-group">
        <label for="inputFrequency" class="col-sm-2 control-label">Exposure:</label>
        <div class="col-sm-2 frequency-wrapper"><input class="no-margin-left pull-left form-control price" id="inputFrequency" value="3"></div>
    </div>

    <div class="form-group">
        <label for="inputDelivery" class="col-sm-2 control-label">Distribution:</label>
        <div class="col-sm-8" style="padding-top:8px">
            <input class="pull-left" style="margin:2px 5px 0 0 !important" type="radio" name="delivery" checked value="evenly"> Evenly throughout the day<br>
            <div class="clearfix" style="height:5px"></div>
            <input class="pull-left" style="margin:2px 5px 0 0 !important" type="radio" name="delivery" value="quick"> As soon as possible
        </div>
    </div>

    <div class="form-group">
        <label for="inputDelivery" class="col-sm-2 control-label">Flight Time:</label>
        <div class="col-sm-8" style="padding-top:8px">
            <input class="pull-left" style="margin:2px 5px 0 0 !important" type="radio" name="stop" checked value="never" onchange="$('#selectdate').hide()"> Run forever (until I stop it)<br>
            <div class="clearfix" style="height:5px"></div>
            <input class="pull-left" style="margin:2px 5px 0 0 !important" type="radio" name="stop" value="date" onchange="$('#selectdate').show()"> End on specific date
            <span id="selectdate" style="display:none">&nbsp;
            <select name="day" class="form-controls selectpicker select-sm" data-size="5">
                <option>Day</option>
                <?php for ($i=1; $i<32 ; $i++) {
                    echo "<option>$i</option>";
                } ?>
            </select>
            <select name="month" class="form-controls selectpicker select-sm" data-size="5">
                <option>Month</option>
                <option>January</option>
                <option>February</option>
                <option>March</option>
                <option>April</option>
                <option>May</option>
                <option>June</option>
                <option>July</option>
                <option>August</option>
                <option>September</option>
                <option>October</option>
                <option>November</option>
                <option>December</option>
            </select>
            <select name="year" class="form-controls selectpicker select-sm" data-size="5">
                <option>Year</option>
                <?php for ($i=date('Y'); $i<date('Y')+11 ; $i++) {
                    echo "<option>$i</option>";
                } ?>
            </select>
            </span>
        </div>
    </div>
    <!-- <h4 class="no-margin-top green">Campaign Delivery</h4> -->


    <div class="clearfix" style="height:15px"></div>
    <hr>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-green">Update Campaign <i class="fa fa-angle-right"></i></button>
        </div>

        <div class="col-sm-offset-2 col-sm-10">
            <br>
            ^^  <strong><u>ON EDIT:</u></strong> Button needs to say "Update Campaign" unless user changes the Landing Page. If LP changes, change the text to "Send for Approval"
        </div>
    </div>
</form>

<?php
require_once('_footer.php');
?>