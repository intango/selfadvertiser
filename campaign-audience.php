<?php
// header
$bodyclass = 'campaign campaign-audience';
require_once('_header.php');

// sidebar
require_once('_sidebar.php');

// top tabs
$active_tab = 'campaign';
require_once('_tabs.php');

$model = (@$_GET['type'] == 'ppc') ? 'CPC' : 'CPV';
?>

<div class="container" id="maincontent">

    <?php require_once('_campaign.php'); ?>

    <!-- tabs -->
    <ul class="nav nav-tabs">
        <li><a href="campaign-kw.php?type=<?php echo @$_GET['type']; ?>">Keywords</a></li>
        <li><a href="campaign-kw-sources.php?type=<?php echo @$_GET['type']; ?>">Sources</a></li>
        <li class="active"><a href="campaign-audience.php?type=<?php echo @$_GET['type']; ?>" onclick="return false;">Audience</a></li>
    </ul>

    <!-- geo table -->
    <div class="col-md-6 no-padding-left">
        <h4 class="pull-left">Geo Statistics</h4>
        <a href="#" class="pull-right" style="margin-top:20px"><i class="fa fa-arrow-circle-down" style="font-size:1em;margin-left:-3px;"></i> Download&nbsp;</a>
        <div class="clearfix"></div>
        <table class="table table-bordered table-hover table-tight" id="listitems">
            <thead>
                <tr class="active">
                    <th class="text-left">Geo</th>
                    <th>Impr.</th>
                    <?php if ($model == 'CPC'): ?>
                    <th>Clicks</th>
                    <th>CTR</th>
                    <?php endif; ?>
                    <th>Cost</th>
                    <th class="text-left">Score</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-il"></span> Israel</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:100%">100</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-us"></span> United States</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:92%">92</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-de"></span> Germany</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:80%">80</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-jp"></span> Japan</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:75%">75</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-tr"></span> Turkey</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:71%">71</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-ca"></span> Canada</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:65%">65</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-es"></span> Spain</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:59%">59</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-gb"></span> United Kiingdom</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:53%">53</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-fr"></span> France</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:41%">41</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-ru"></span> Russia</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:32%">32</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-se"></span> Sweden</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:32%">32</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-pt"></span> Portugal</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:24%">24</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-lu"></span> Luxembourg</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:23%">23</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-jo"></span> Jordan</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:18%">18</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-kp"></span> Korea</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:12%">12</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-md"></span> Moldova</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:5%">5</div></th>
                </tr>
                <tr>
                    <td class="text-left nowrap"><span class="flag flag-eg"></span> Egypt</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:1%">1</div></th>
                </tr>
            </tbody>
        </table>
    </div>

    <!-- devices -->
    <div class="col-md-6 no-padding">

        <h4 class="pull-left">Desktops Statistics</h4>
        <a href="#" class="pull-right" style="margin-top:20px"><i class="fa fa-arrow-circle-down" style="font-size:1em;margin-left:-3px;"></i> Download&nbsp;</a>
        <div class="clearfix"></div>
        <table class="table table-bordered table-hover table-tight" id="listitems">
            <thead>
                <tr class="active">
                    <th class="check hidden-xs"><i class="fa fa-circle status-gray tooltip-auto" title="Desktops"></i></th>
                    <th class="text-left">Browser</th>
                    <th>Impr.</th>
                    <?php if ($model == 'CPC'): ?>
                    <th>Clicks</th>
                    <th>CTR</th>
                    <?php endif; ?>
                    <th>Cost</th>
                    <th class="text-left">Score</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="check hidden-xs"><i class="fa fa-laptop"></i></td>
                    <td class="text-left nowrap"><span class="browser browser-chrome"></span> Chrome</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:100%">100</div></th>
                </tr>
                <tr>
                    <td class="check hidden-xs"><i class="fa fa-laptop"></i></td>
                    <td class="text-left nowrap"><span class="browser browser-firefox"></span> Firefox</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:92%">92</div></th>
                </tr>
                <tr>
                    <td class="check hidden-xs"><i class="fa fa-laptop"></i></td>
                    <td class="text-left nowrap"><span class="browser browser-msie"></span> Internet Explorer</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:80%">80</div></th>
                </tr>
                <tr>
                    <td class="check hidden-xs"><i class="fa fa-laptop"></i></td>
                    <td class="text-left nowrap"><span class="browser browser-safari"></span> Safari</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:30%">80</div></th>
                </tr>
                <tr>
                    <td class="check hidden-xs"><i class="fa fa-laptop"></i></td>
                    <td class="text-left nowrap"><span class="browser browser-opera"></span> Opera</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:10%">80</div></th>
                </tr>
                <tr>
                    <td class="check hidden-xs"><i class="fa fa-laptop"></i></td>
                    <td class="text-left nowrap"><span class="browser browser-other"></span> Other</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:5%">80</div></th>
                </tr>
            </tbody>
        </table>


        <h4 class="pull-left">Tablet Statistics</h4>
        <a href="#" class="pull-right" style="margin-top:20px"><i class="fa fa-arrow-circle-down" style="font-size:1em;margin-left:-3px;"></i> Download&nbsp;</a>
        <div class="clearfix"></div>
        <table class="table table-bordered table-hover table-tight" id="listitems">
            <thead>
                <tr class="active">
                    <th class="check hidden-xs"><i class="fa fa-circle status-gray tooltip-auto" title="Tablets"></i></th>
                    <th class="text-left">Platform</th>
                    <th>Impr.</th>
                    <?php if ($model == 'CPC'): ?>
                    <th>Clicks</th>
                    <th>CTR</th>
                    <?php endif; ?>
                    <th>Cost</th>
                    <th class="text-left">Score</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="check hidden-xs"><i class="fa fa-tablet"></i></td>
                    <td class="text-left nowrap"><span class="os os-ios"></span> iOS</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:100%">100</div></th>
                </tr>
                <tr>
                    <td class="check hidden-xs"><i class="fa fa-tablet"></i></td>
                    <td class="text-left nowrap"><span class="os os-android"></span> Android</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:98%">98</div></th>
                </tr>
            </tbody>
        </table>

        <h4 class="pull-left">Smartphones Statistics</h4>
        <a href="#" class="pull-right" style="margin-top:20px"><i class="fa fa-arrow-circle-down" style="font-size:1em;margin-left:-3px;"></i> Download&nbsp;</a>
        <div class="clearfix"></div>
        <table class="table table-bordered table-hover table-tight" id="listitems">
            <thead>
                <tr class="active">
                    <th class="check hidden-xs"><i class="fa fa-circle status-gray tooltip-auto" title="Smartphones"></i></th>
                    <th class="text-left">Platform</th>
                    <th>Impr.</th>
                    <?php if ($model == 'CPC'): ?>
                    <th>Clicks</th>
                    <th>CTR</th>
                    <?php endif; ?>
                    <th>Cost</th>
                    <th class="text-left">Score</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="check hidden-xs"><i class="fa fa-mobile"></i></td>
                    <td class="text-left nowrap"><span class="os os-android"></span> Android</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:100%">100</div></th>
                </tr>
                <tr>
                    <td class="check hidden-xs"><i class="fa fa-mobile"></i></td>
                    <td class="text-left nowrap"><span class="os os-ios"></span> iOS</td>
                    <td>12,345</td>
                    <?php if ($model == 'CPC'): ?>
                    <td>1,234</td>
                    <td>10.00%</td>
                    <td>$123.40</td>
                    <?php else: ?>
                    <td>$123.45</td>
                    <?php endif ;?>
                    <th class="text-left text-middle score"><div class="bar" style="width:92%">92</div></th>
                </tr>
            </tbody>
        </table>
    </div>

<?php
require_once('_footer.php');
?>