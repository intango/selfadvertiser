<!-- /Content area -->
</div>
</div>

<div id="footer"><div class="container light">
    <div class="pull-left">
        &copy; 2014 RevenueHits<span class="hidden-xs">. All rights reserved to MyAdWise Ltd.</span><br>
        <a href="./legal.php?title=advertising+guidelines"><span class="hidden-xs">Advertising</span> Guidelines</a> &nbsp;
        <a href="./legal.php?title=terms+of+service">Terms<span class="hidden-xs"> of Service</span></a> &nbsp;
        <a href="./legal.php?title=privacy+policy">Privacy<span class="hidden-xs"> Policy</span></a> &nbsp;
        <a href="./support-form.php">Support</a>
    </div>
    <div class="pull-right text-right text-sm">
        Reporting is not real-time. Clicks and impressions received today are not included here.<br class="hidden-xs">
        Time zone for all dates and times: (GMT-05:00) Eastern Time.
    </div>
    <div class="clearfix"></div>
</div></div>

<script>
var viewRange = {
    "start": moment().subtract(8, 'days').format('YYYY-MM-DD'),
    "end": moment().subtract(1, 'days').format('YYYY-MM-DD')
};
$('#prettyrange').html(moment().subtract(8, 'days').format('MMMM D, YYYY') +' - '+ moment().subtract(1, 'days').format('MMMM D, YYYY'));
$('#mobile-daterage').html($('#prettyrange').html());

</script>

</body>
</html>