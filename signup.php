<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="robots" content="noindex">
    <link rel="shortcut icon" href="./favicon.ico">

    <title>Sign up as an advertiser</title>

    <!-- Bootstrap + Custom CSS -->
    <link href="./media/css/bootstrap.css" rel="stylesheet">
    <link href="./media/css/bootstrap-theme.css" rel="stylesheet">
    <link href="./media/css/font-awesome.css" rel="stylesheet">
    <link href="./media/css/font-proxima.css" rel="stylesheet">
    <link href="./media/css/icons.css" rel="stylesheet">
    <link href="./media/css/layout.css" rel="stylesheet">

    <!-- Bootstrap + jQuery + Custom JS -->
    <script src="./media/js/jquery.min.js" ></script>
    <script src="./media/js/bootstrap.min.js"></script>

    <!--  date picker -->
    <link href="./media/css/daterangepicker.css" rel="stylesheet">
    <script src="./media/js/moment.js"></script>
    <script src="./media/js/daterangepicker.js" ></script>

    <!-- chart stuff -->
    <link rel="stylesheet" href="./media/css/morris.css" rel="stylesheet">
    <script src="./media/js/raphael-min.js"></script>
    <script src="./media/js/morris.min.js"></script>

    <!-- touchspin -->
    <link rel="stylesheet" href="./media/css/touchspin.min.css" rel="stylesheet">
    <script src="./media/js/touchspin.min.js"></script>

    <!-- advanced select -->
    <link rel="stylesheet" href="./media/css/bootstrap-select.min.css" rel="stylesheet">
    <script src="./media/js/bootstrap-select.min.js"></script>

    <!-- SweetAlert - alert replacer -->
    <link href="./media/css/sweet-alert.css" rel="stylesheet">
    <script src="./media/js/sweet-alert.min.js" ></script>

    <!-- NProgress - Progress Bar -->
    <link href="./media/css/nprogress.css" rel="stylesheet">
    <script src="./media/js/nprogress.js" ></script>

    <!-- main Javascript -->
    <script src="./media/js/common.js"></script>
    <script src="./media/js/browsercheck.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="./media/js/html5shiv.min.js"></script>
        <script src="./media/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="signup">
<script> NProgress.start(); </script>
<div id="heros"></div>

<div id="wrap"><div class="container">
    <div class="navbar-brand"></div>

    <form class="form-signup col-md-5" action="./create-campaign.php?first=1" method="post" id="signup-form" style="max-width:560px">

        <h1 class="form-signin-heading green">One-Page Signup</h1>
        <h3>Become an Superhero Advertiser today</h3>
        <!-- <p class="alert alert-danger">Some error message if occured</p> -->
        &nbsp;

        <div class="form-group">
            <label for="inputFName">Full Name:</label>
            <div>
                <div class="col-md-5 no-padding-left">
                    <input type="text" class="form-control" id="inputFName" placeholder="First">
                </div>
                <div class="col-md-7 no-padding">
                    <input type="text" class="form-control" id="inputLName" placeholder="Last">
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <label for="inputEmail">Primary Email Address:</label>
            <input type="email" class="form-control" id="inputEmail" placeholder="Email">
        </div>

        <div class="form-group has-error">
            <label for="inputName">Country:</label>
            <select class="form-control selectpicker" data-live-search="true">
                <?php
                include('_countries.php');
                foreach ($FLATDB_GEOS_CODES as $code=>$name){
                    echo '<option value="'.$code.'"';
                    if ($code == 'IL') echo ' selected';
                    echo '>'.$name.'</option>';
                }
                ?>
            </select>
        </div>

        <div class="form-group">
            <label for="inputPhone">Phone #:</label>
            <input type="phone" class="form-control" id="inputPhone" placeholder="+972">
        </div>
        &nbsp;

        <h3 class="green simple" style="margin-bottom:20px">Login information</h3>


        <div class="form-group has-error">
            <label for="inputUserName">Choose a Username:</label>
            <input type="text" class="form-control" id="inputUserName" placeholder="username">
            <p class="maroon"><i class="fa fa-warning"></i> Error message goes here</p>
        </div>

        <div class="form-group">
            <label for="inputUserName">Create Password:</label>
            <div>
                <div class="col-md-5 no-padding-left">
                    <input type="password" class="form-control" id="inputUserName" placeholder="password">
                </div>
                <div class="col-md-7 no-padding">
                    <input type="password" class="form-control" id="inputUserName" placeholder="confirm password">
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <br><hr>
        <p><span class="has-error"><input type="checkbox"></span> I agree to the <a target="rhlegal" href="./legal.php?title=terms+of+service">Terms of Service</a> and <a target="rhlegal" href="./legal.php?title=advertising+guidelines">Advertising Guidelines</a></p>
        <hr>

        <div class="col-md-6 no-padding-left"><br>
            <button class="btn btn-lg btn-green btn-block" style="font:18px/1.6 arial" id="submit-btn" type="submit">Sign Up</button>
        </div>


        <div class="clearfix"></div>
        <div style="margin-top:100px">
            <hr>
            <p class="text-muted">
                &copy; 2014 RevenueHits. All rights reserved to MyAdWise Ltd.</span><br>
                <a target="rhlegal" href="../legal.php?title=advertising+guidelines"><span class="hidden-xs">Advertising</span> Guidelines</a> &nbsp;
                <a target="rhlegal" href="./legal.php?title=terms+of+service">Terms<span class="hidden-xs"> of Service</span></a> &nbsp;
                <a target="rhlegal" href="./legal.php?title=privacy+policy">Privacy<span class="hidden-xs"> Policy</span></a> &nbsp;
                <a href="./support-form.php">Support</a>
            </p>
        </div>

   </form>
</div></div>


</body>
</html>