<?php
// header
$bodyclass = 'notice campaign-group';
require_once('_header.php');

// sidebar
require_once('_sidebar.php');

// top tabs
$active_tab = 'all';
require_once('_tabs.php');
?>

<div class="container" id="maincontent">

    <!-- main chart -->
    <div id="mobile-daterage"></div>

    <!-- mini charts -->
    <?php require_once('_minicharts.php'); ?>

    <!-- toolbar -->
    <div class="clearfix"></div>
    <div id="toolbar">
        <div class="dropdown main-button inline pull-left">
            <a href="#" class="dropdown-toggle btn btn-green" data-toggle="dropdown"><i class="fa fa-plus"></i> New Campaign <i class="fa fa-caret-down"></i></a>
            <ul class="dropdown-menu arrow-left">
                <li><a href="create-campaign.php?model=pop"><i class="fa fa-external-link"></i>&nbsp; Pop/Interstitial</a></li>
                <li><a href="create-campaign.php?model=domain-redirect"><i class="fa fa-bolt" style="margin:0 3px 0 2px"></i>&nbsp; Domain Redirect</a></li>
                <li><a href="create-campaign.php?model=search"><i class="fa fa-search"></i>&nbsp; Search Campaign</a></li>
            </ul>
            &nbsp;
            <div class="dropdown inline">
                <a href="#" id="toolbar_actions" class="dropdown-toggle btn btn-silver disabled" data-toggle="dropdown">Actions <i class="fa fa-caret-down"></i></a>
                <ul class="dropdown-menu arrow-left">
                    <li><a href="#" onclick="return rusure(this)"><i class="fa fa-remove status-red"></i> Remove</a></li>
                    <li><a href="#" onclick="return rusure(this)"><i class="fa fa-file status-gray"></i> Clone</a></li>
                </ul>
            </div>
        </div>
        <div class="pull-left">
            <a href="#" class="btn btn-silver"><i class="fa fa-arrow-circle-down" style="font-size:1em;margin-left:-3px;"></i> &nbsp;Download&nbsp;</a>
        </div>

        <div class="pull-left" style="padding-top:9px">
            &nbsp; &nbsp; &nbsp;
            <a class="checkboxed" href="#" id="toggle_removed">Show removed &amp; rejected campaigns</a>
        </div>
        <div class="clearfix"></div>
    </div>

    <p class="alert alert-warning" role="alert"><i class="fa icon fa-warning"></i> Your campaigns aren't getting the traffic because your you ran out of budget.</p>

    <!-- main table -->
    <table class="table table-bordered table-hover" id="listitems">
        <thead>
            <tr class="active">
                <th class="check"><input type="checkbox"></th>
                <th class="link hidden-xs"><a name="extlink" class="tooltip-right" title="Open landing page in a new tab"><i class="fa fa-link"></i></a></th>
                <th class="status hidden-xs"><i class="fa fa-circle status-gray"></i></th>
                <th class="text-left">Campaign</th>
                <th class="text-left">Type</th>
                <th class="text-left hidden-xs">Targeting</th>
                <th class="text-left">Status</th>
                <th><span class="visible-xs">Daily </span>Budget</th>
                <th>Cost</th>
                <th class="rightdiv">Traffic</th>
                <th>Clicks</th>
                <th>CTR</th>
                <th class="rightdiv">CPC</th>
                <th class="rightdiv">CPV</th>
                <th>Conv.</th>
                <th>Conv. %</th>
                <th>Conv. $</th>
                <th>$/Visit</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="check"><input type="checkbox"></td>
                <td class="link hidden-xs"><a href="#" target="_blank"><i class="fa fa-link"></i></a></td>
                <td class="status hidden-xs"><div class="dropdown inline">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-circle status-green tooltip-auto" title="Running since Aug 12, 2014"></i></a>
                    <ul class="dropdown-menu arrow-left">
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-pause status-gray"></i> Pause</a></li>
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-remove status-red"></i> Remove</a></li>
                        <li class="divider"></li>
                        <li><a href="campaign-ron.php"><i class="fa fa-area-chart status-orange"></i> Reports</a></li>
                        <li class="divider"></li>
                        <li><a href="edit-campaign-ppc.php"><i class="fa fa-pencil status-gray"></i> Edit</a></li>
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-file status-gray"></i> Clone</a></li>
                    </ul>
                </div></td>
                <td class="text-left"><a href="campaign-ron.php?type=ppc" class="settings">Base offer</a></td>
                <td class="text-left">Search</td>
                <td class="text-left hidden-xs">Run of Network</td>
                <td class="text-left"><span class="marker marker-green">Running</span></td>
                <td>$25.00<span class="hidden-xs">/day</span></td>
                <td>$123.40</td>
                <td class="rightdiv">12,345</td>
                <td>1,234</td>
                <td>10.00%</td>
                <td class="rightdiv">$0.10</td>
                <td class="rightdiv light-text">--</td>
                <td>1,234</td>
                <td>10.00%</td>
                <td>$123.40</td>
                <td>$0.10</td>
            </tr>
            <tr>
                <td class="check"><input type="checkbox"></td>
                <td class="link hidden-xs"><a href="#" target="_blank"><i class="fa fa-link"></i></a></td>
                <td class="status hidden-xs"><div class="dropdown inline">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-circle status-green tooltip-auto" title="Running since Sep 27, 2014"></i></a>
                    <ul class="dropdown-menu arrow-left">
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-pause status-gray"></i> Pause</a></li>
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-remove status-red"></i> Remove</a></li>
                        <li class="divider"></li>
                        <li><a href="campaign-kw.php"><i class="fa fa-area-chart status-orange"></i> Reports</a></li>
                        <li class="divider"></li>
                        <li><a href="edit-campaign.php"><i class="fa fa-pencil status-gray"></i> Edit</a></li>
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-file status-gray"></i> Clone</a></li>
                    </ul>
                </div></td>
                <td class="text-left"><a href="campaign-kw.php" class="settings">Yellow background LP</a></td>
                <td class="text-left">Pop</td>
                <td class="text-left hidden-xs">Keyword-Targeted</td>
                <td class="text-left"><span class="marker marker-green">Running</span></td>
                <td>$10.00<span class="hidden-xs">/day</span></td>
                <td>$3.21</td>
                <td class="rightdiv">321</td>
                <td class="light-text">--</td>
                <td class="light-text">--</td>
                <td class="rightdiv light-text">--</td>
                <td class="rightdiv">$0.01</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>$0.00</td>
            </tr>
            <tr class="hidden-row">
                <td class="check"><input type="checkbox"></td>
                <td class="link hidden-xs"><a href="#" target="_blank"><i class="fa fa-link"></i></a></td>
                <td class="status hidden-xs"><a href="edit-campaign.php"><i class="fa fa-warning status-maroon tooltip-auto" title="Click for more information"></i></a></td>
                <td class="text-left"><a href="edit-campaign.php?type=ppc" class="settings">Gray background LP</a></td>
                <td class="text-left">Search</td>
                <td class="text-left hidden-xs">Run of Network</td>
                <td class="text-left"><span class="marker marker-maroon">Rejected</span></td>
                <td>$10.00<span class="hidden-xs">/day</span></td>
                <td>$3.21</td>
                <td class="rightdiv">321</td>
                <td>32</td>
                <td>10.00%</td>
                <td class="rightdiv">$0.10</td>
                <td class="rightdiv light-text">--</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>$0.00</td>
            </tr>
            <tr class="hidden-row">
                <td class="check"><input type="checkbox"></td>
                <td class="link hidden-xs"><a href="#" target="_blank"><i class="fa fa-link"></i></a></td>
                <td class="status hidden-xs"><i class="fa fa-remove status-red tooltip-auto" title="You removed this campaign on Oct 10, 2014"></i></td>
                <td class="text-left"><a href="campaign-kw.php" class="settings">Blue background LP</a></td>
                <td class="text-left">Pop</td>
                <td class="text-left hidden-xs">Keyword-Targeted</td>
                <td class="text-left"><span class="marker marker-red">Removed</span></td>
                <td>$10.00<span class="hidden-xs">/day</span></td>
                <td>$3.21</td>
                <td class="rightdiv">321</td>
                <td class="light-text">--</td>
                <td class="light-text">--</td>
                <td class="rightdiv light-text">--</td>
                <td class="rightdiv">$0.01</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td class="check"><input type="checkbox"></td>
                <td class="link hidden-xs"><a href="#" target="_blank"><i class="fa fa-link"></i></a></td>
                <td class="status hidden-xs"><i class="fa fa-circle status-orange tooltip-auto" title="Awaiting approval by our team"></i></td>
                <td class="text-left"><a href="campaign-kw.php?type=ppc" class="settings">Family smiling</a></td>
                <td class="text-left">Search</td>
                <td class="text-left hidden-xs">Keyword-Targeted</td>
                <td class="text-left"><span class="marker marker-orange">Pending</span></td>
                <td>$10.00<span class="hidden-xs">/day</span></td>
                <td>$51.70</td>
                <td class="rightdiv">5,172</td>
                <td>517</td>
                <td>10.00%</td>
                <td class="rightdiv">$0.10</td>
                <td class="rightdiv light-text">--</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td class="check"><input type="checkbox"></td>
                <td class="link hidden-xs"><a href="#" target="_blank"><i class="fa fa-link"></i></a></td>
                <td class="status hidden-xs"><div class="dropdown inline">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-circle status-gray"></i></a>
                    <ul class="dropdown-menu arrow-left">
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-play status-green"></i> Resume</a></li>
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-remove status-red"></i> Remove</a></li>
                        <li class="divider"></li>
                        <li><a href="campaign-kw.php"><i class="fa fa-area-chart status-orange"></i> Reports</a></li>
                        <li class="divider"></li>
                        <li><a href="edit-campaign-ppc.php"><i class="fa fa-pencil status-gray"></i> Edit</a></li>
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-file status-gray"></i> Clone</a></li>
                    </ul>
                </div></td>
                <td class="text-left"><a href="campaign-kw.php?type=ppc" class="settings">Family smiling</a></td>
                <td class="text-left">Search</td>
                <td class="text-left hidden-xs">Keyword-Targeted</td>
                <td class="text-left"><span class="marker marker-gray">Paused</span></td>
                <td>$10.00<span class="hidden-xs">/day</span></td>
                <td>$51.70</td>
                <td class="rightdiv">5,172</td>
                <td>517</td>
                <td>10.00%</td>
                <td class="rightdiv">$0.10</td>
                <td class="rightdiv light-text">--</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td class="check"><input type="checkbox"></td>
                <td class="link hidden-xs"><a href="#" target="_blank"><i class="fa fa-link"></i></a></td>
                <td class="status hidden-xs"><div class="dropdown inline">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-warning status-orange tooltip-auto" title="Out of budget"></i></a>
                    <ul class="dropdown-menu arrow-left">
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-pause status-gray"></i> Pause</a></li>
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-remove status-red"></i> Remove</a></li>
                        <li class="divider"></li>
                        <li><a href="campaign-kw.php"><i class="fa fa-area-chart status-orange"></i> Reports</a></li>
                        <li class="divider"></li>
                        <li><a href="edit-campaign.php"><i class="fa fa-pencil status-gray"></i> Edit</a></li>
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-file status-gray"></i> Clone</a></li>
                    </ul>
                </div></td>
                <td class="text-left"><a href="campaign-ron.php" class="settings">Gray background LP</a></td>
                <td class="text-left">Pop</td>
                <td class="text-left hidden-xs">Run of Network</td>
                <td class="text-left"><span class="marker marker-dirt">Out of budget</span></td>
                <td>$10.00<span class="hidden-xs">/day</span></td>
                <td>$3.21</td>
                <td class="rightdiv">321</td>
                <td class="light-text">--</td>
                <td class="light-text">--</td>
                <td class="rightdiv light-text">--</td>
                <td class="rightdiv">$0.01</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td class="check"><input type="checkbox"></td>
                <td class="link hidden-xs"><a href="#" target="_blank"><i class="fa fa-link"></i></a></td>
                <td class="status hidden-xs"><div class="dropdown inline">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-warning status-orange tooltip-auto" title="Out of budget"></i></a>
                    <ul class="dropdown-menu arrow-left">
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-pause status-gray"></i> Pause</a></li>
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-remove status-red"></i> Remove</a></li>
                        <li class="divider"></li>
                        <li><a href="campaign-kw.php"><i class="fa fa-area-chart status-orange"></i> Reports</a></li>
                        <li class="divider"></li>
                        <li><a href="edit-campaign.php"><i class="fa fa-pencil status-gray"></i> Edit</a></li>
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-file status-gray"></i> Clone</a></li>
                    </ul>
                </div></td>
                <td class="text-left"><a href="campaign-ron.php" class="settings">Orange background LP</a></td>
                <td class="text-left">Domain Redirect</td>
                <td class="text-left hidden-xs">Run of Network</td>
                <td class="text-left"><span class="marker marker-black">Ended</span></td>
                <td>$10.00<span class="hidden-xs">/day</span></td>
                <td>$3.21</td>
                <td class="rightdiv">321</td>
                <td class="light-text">--</td>
                <td class="light-text">--</td>
                <td class="rightdiv light-text">--</td>
                <td class="rightdiv">$0.01</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td class="check"><input type="checkbox"></td>
                <td class="link hidden-xs"><a href="#" target="_blank"><i class="fa fa-link"></i></a></td>
                <td class="status hidden-xs"><div class="dropdown inline">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-warning status-orange tooltip-auto" title="Out of budget"></i></a>
                    <ul class="dropdown-menu arrow-left">
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-pause status-gray"></i> Pause</a></li>
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-remove status-red"></i> Remove</a></li>
                        <li class="divider"></li>
                        <li><a href="campaign-kw.php"><i class="fa fa-area-chart status-orange"></i> Reports</a></li>
                        <li class="divider"></li>
                        <li><a href="edit-campaign.php"><i class="fa fa-pencil status-gray"></i> Edit</a></li>
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-file status-gray"></i> Clone</a></li>
                    </ul>
                </div></td>
                <td class="text-left"><a href="campaign-ron.php" class="settings">Purple background LP</a></td>
                <td class="text-left">Domain Redirect</td>
                <td class="text-left hidden-xs">Run of Network</td>
                <td class="text-left"><span class="marker marker-purple">Met Daily Budget</span></td>
                <td>$10.00<span class="hidden-xs">/day</span></td>
                <td>$3.21</td>
                <td class="rightdiv">321</td>
                <td class="light-text">--</td>
                <td class="light-text">--</td>
                <td class="rightdiv light-text">--</td>
                <td class="rightdiv">$0.01</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>$0.00</td>
            </tr>
        </tbody>
    </table>
    <p class="light-text"><strong style="color:#c00">*</strong> Clicks, CTR and CPC are calculated for Search-based campaigns. CPV is calculated for Pop and Domain Redirect campaigns.</p>



<?php
require_once('_pagination.php');
require_once('_footer.php');
?>