<?php
// header
$bodyclass = 'support';
require_once('_header.php');
?>

<!-- header w/ tabs -->
<div id="header" class="container-max">
    <h2>Help Center</h2>
    <div class="clearfix"></div>
    <ul class="nav nav-tabs">
        <li><a href="support.php">Knowledge Base</a></li>
        <li class="active"><a href="support-form.php">Support Desk</a></li>
    </ul>
</div>

<div class="container" id="maincontent">

<form class="form-horizontal" role="form">
    <h3 class="no-margin-top green simple">Contact Us</h3>
    <p>If you've tried everything and still feeling stupid, please contact us. We promise we won't laugh at you...</p>
    <hr>
    <!-- message -->
    <p class="alert no-margin-top alert-success" role="alert"><i class="fa icon fa-warning"></i> Your question was submitted successfuly. You can expect an answer within 24 hours!</p>


    <!-- account info -->
    <div class="col-md-6 no-padding">

        <div class="form-group">
            <label for="inputAddress">Your question:</label>
            <textarea name="address" id="inputAddress" class="form-control" rows="10"></textarea>
        </div>

        <p>&nbsp;</p>
        <p>
            <button type="submit" class="btn btn-green">Send Question <i class="fa fa-angle-right"></i></button>
        </p>
    </div>

    <div class="clearfix"></div>
    <p>&nbsp;</p>

</form>

<?php
require_once('_footer.php');
?>