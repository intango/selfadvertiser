<!-- toolbar -->
<div id="toolbar">
    <div class="main-button inline">
        <div class="dropdown inline">
            <a href="#" id="toolbar_actions" class="dropdown-toggle btn btn-silver btn-sm disabled" data-toggle="dropdown">Actions <i class="fa fa-caret-down"></i></a>
            <ul class="dropdown-menu arrow-left" style="width:220px">
                <?php if (@$show_block): ?><li><a href="#" onclick="return rusure(this)"><i class="fa fa-remove status-red"></i> Block selected</a></li><?php endif; ?>
                <li><a href="campaign-create-campaign.php"><i class="fa fa-plus status-green"></i> New campaign from selected</a></li>
            </ul>
        </div>
    </div>

    <?php if (@$show_block): ?>
        <a href="#manage-inline-lists" onclick="return toggleInlineList();" class="btn btn-silver btn-sm">Manage Sources <i class="fa fa-angle-double-down" id="show-inline-lists"></i></a>
        &nbsp;
        <a href="#" class="btn btn-silver btn-sm"><i class="fa fa-arrow-circle-down" style="font-size:1em;margin-left:-3px;"></i> &nbsp;Download&nbsp;</a>

        <form id="manage-inline-lists" style="margin-top:10px; display:none">
            <p class="bg-danger">Sample error message for when something goes wrong upon saving</p>
            <div class="col-md-3 no-padding-left">
                <h5><i class="fa fa-ban red"></i> Black-list Source IDs</h5>
                <textarea name="blacklist" id="sourceid-text" class="form-control blacklist" rows="10">912312
12973
134234
986323
</textarea>
            <h3>-- OR --</h3>
            <h5><i class="fa fa-check green"></i> White-list Source IDs</h5>
            <textarea name="whitelist" id="sourceid-text" class="form-control whitelist" rows="10">912312
12973
134234
986323
</textarea>
            </div>
            <div class="col-md-2 no-padding-left">
                <h5>&nbsp;</h5>
                <button class="btn btn-green">Update Sources List</button>
            </div>
            <div class="clearfix"></div><hr>
        </form>
    <?php endif; ?>

    <?php if (@$show_keywords): ?>
        <a href="#manage-inline-lists" onclick="return toggleInlineList();" class="btn btn-silver btn-sm">Manage Keywords <i class="fa fa-angle-double-down" id="show-inline-lists"></i></a>
        &nbsp;
        <a href="#" class="btn btn-silver btn-sm"><i class="fa fa-arrow-circle-down" style="font-size:1em;margin-left:-3px;"></i> &nbsp;Download&nbsp;</a>

        <form id="manage-inline-lists" style="margin-top:10px; display:none">
            <p class="alert alert-danger">Sample error message for when something goes wrong upon saving</p>
            <div class="col-md-3 no-padding-left">
                <h5><i class="fa fa-check green"></i> Active Keywords</h5>
                <textarea name="keywords" class="form-control" rows="15">shopping
amazon.com/*/shopping
</textarea>
            </div>
            <div class="col-md-3 no-padding-left">
                <h5><i class="fa fa-ban red"></i> Blocked Keywords</h5>
                <textarea name="keywords-blocked" class="form-control" rows="15">cheap
coupons
</textarea>
            </div>
            <div class="col-md-2 no-padding-left">
                <h5>&nbsp;</h5>
                <button class="btn btn-green">Update Keywords List</button>
            </div>
            <div class="clearfix"></div><hr>
        </form>
    <?php endif; ?>


</div>