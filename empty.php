<?php

// header
$bodyclass = 'campaign-group';
require_once('_header.php');

// top tabs
$group_view = true;
$active_tab = 'all';
require_once('_tabs.php');
?>
<script> $('#reportrange').hide(); </script>
<div class="container" id="maincontent">

    <div id="getting-started">
        <div class="start-bubble-2"></div>
        <div class="steps">
            <h3>Welcome... Let's get to work!</h3>
            <div>
                <dl class="step done">
                    <dt><i class="fa fa-check-circle"></i></dt>
                    <dd><h4>Create your account</h4> Signup with SelfAdvertiser.com. It only takes a few seconds.</dd>
                </dl>
                <dl class="step">
                    <dt><span class="active">2</span></dt>
                    <dd><h4><a href="./create-campaign.php">Create your first campaign</a></h4> Enter your site's URL, select your audience and set your budget.</dd>
                </dl>
                <dl class="step">
                    <dt><span>3</span></dt>
                    <dd><h4>Fund your account</h4> Deposit a upwards of $200 using Paypal or credit card, or wire.</dd>
                </dl>
                <dl class="step">
                    <dt><span>4</span></dt>
                    <dd><h4>Go live!</h4> Sit back and watch your traffic grows.</dd>
                </dl>
                <p>&nbsp;</p>
            <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<?php

require_once('_footer.php');
?>
