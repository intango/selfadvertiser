<?php
// header
$bodyclass = 'campaign campaign-kw';
require_once('_header.php');

// sidebar
require_once('_sidebar.php');

// top tabs
$active_tab = 'campaign';
require_once('_tabs.php');


$model = (@$_GET['type'] == 'ppc') ? 'CPC' : 'CPV';
?>
<div class="container" id="maincontent">

    <?php require_once('_campaign.php'); ?>

    <!-- tabs -->
    <ul class="nav nav-tabs">
        <li class="active"><a href="campaign-kw.php?type=<?php echo @$_GET['type']; ?>" onclick="return false;">Keywords</a></li>
        <li><a href="campaign-kw-sources.php?type=<?php echo @$_GET['type']; ?>">Sources</a></li>
        <li><a href="campaign-audience.php?type=<?php echo @$_GET['type']; ?>">Audience</a></li>
    </ul>

    <?php
    $show_block = false;
    $show_keywords = true;
    require_once('_campaign-toolbar.php');
    ?>

    <!-- main table -->
    <table class="table table-bordered table-hover" id="listitems">
        <thead>
            <tr class="active">
                <th class="check"><input type="checkbox"></th>
                <th class="status hidden-xs"><i class="fa fa-circle status-gray"></i></th>
                <th class="text-left">Keyword</th>
                <th>Impr.</th>
                <?php if ($model == 'CPC'): ?>
                <th>Clicks</th>
                <th>CTR</th>
                <?php endif; ?>
                <th>Cost</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="check"><input type="checkbox"></td>
                <td class="status hidden-xs"><div class="dropdown inline">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-circle status-green"></i></a>
                    <ul class="dropdown-menu arrow-left" style="width:220px">
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-ban status-red"></i> Block keyword</a></li>
                        <li><a href="campaign-create-campaign.php"><i class="fa fa-plus status-green"></i> New campaign from keyword</a></li>
                    </ul>
                </div></td>
                <td class="text-left">shopping</td>
                <td>12,345</td>
                <?php if ($model == 'CPC'): ?>
                <td>1,234</td>
                <td>10.00%</td>
                <td>$123.40</td>
                <?php else: ?>
                <td>$123.45</td>
                <?php endif ;?>
            </tr>
            <tr>
                <td class="check"><input type="checkbox"></td>
                <td class="status hidden-xs"><div class="dropdown inline">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-circle status-green"></i></a>
                    <ul class="dropdown-menu arrow-left" style="width:220px">
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-ban status-red"></i> Block keyword</a></li>
                        <li><a href="campaign-create-campaign.php"><i class="fa fa-plus status-green"></i> New campaign from keyword</a></li>
                    </ul>
                </div></td>
                <td class="text-left">amazon.com/*/shopping</td>
                <td>12,345</td>
                <?php if ($model == 'CPC'): ?>
                <td>1,234</td>
                <td>10.00%</td>
                <td>$123.40</td>
                <?php else: ?>
                <td>$123.45</td>
                <?php endif ;?>
            </tr>
        </tbody>
    </table>

<?php
require_once('_pagination.php');
require_once('_footer.php');
?>