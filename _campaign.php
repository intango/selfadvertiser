<?php
$model = (@$_GET['type'] == 'ppc') ? 'CPC' : 'CPV';
?>
    <!-- campaign info -->
    <div class="col-md-5 no-padding-left">
        <h3 class="pull-left no-margin-top">Active Campaign</h3>
        <a href="edit-campaign.php" class="edit-link-h3"><i class="fa fa-gears tooltip-auto" title="Edit Campaign Settings" onclick="window.location=this.parentNode.href"></i> edit</a>
        <div class="clearfix"></div>

        <table class="info-table">
            <tr>
                <th>Status:</th>
                <td><div class="dropdown inline">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="green">Running <i class="fa fa-angle-down"></i></span></a>
                    <ul class="dropdown-menu arrow-left">
                        <li><a href="#"><i class="fa fa-play status-green"></i> <strong>Running</strong></a></li>
                        <li><a href="#" onclick="return rusure('Are you sure you want to pause this offer?')"><i class="fa fa-pause status-gray"></i> Pause</a></li>
                        <li><a href="#" onclick="return rusure('Are you sure you want to completely stop this offer?')"><i class="fa fa-stop status-red"></i> Stop</a></li>
                    </ul>
                </div></td>
                <th>Exposure:</th>
                <td>3 /day /user</td>
            </tr>
            <tr>
                <th>Budget: &nbsp;<i class="gray fa fa-pencil tooltip-bottom" title="Click on the prices to edit"></i></th>
                <td class="budget-group" style="width:160px"><input class="blend budget" name="budget" value="$25.00" onclick="budgetify(1, this);"></td>
                <th><?php echo $model; ?>:</th>
                <td class="budget-group" style="width:160px"><input class="blend budget" name="cpv" value="$<?php echo ($model=='CPC') ? '0.10' : '0.01'; ?>" onclick="cpvify(1, this);"></td>
            </tr>
            <tr>
                <th>Destination URL:</th>
                <td colspan="3"><a href="#">http://www.revenuehits.com/lps/v1/?ref=%%token%%</a></td>
            </tr>
            <tr>
                <th>Targeting:</th>
                <td colspan="3" style="zoom:1.1;">
                    <i class="fa fa-laptop tooltip-auto" title="Desktops &amp; Laptops"></i>
                    &nbsp;<i class="fa fa-tablet tooltip-auto" title="Tablets"></i>
                    &nbsp;<i class="fa fa-mobile-phone tooltip-auto" title="Smartphones"></i>
                    <span class="lighter">&nbsp; |&nbsp;</span>
                    &nbsp;<i class="fa fa-windows tooltip-auto" title="Windows"></i>
                    &nbsp;<i class="fa fa-android tooltip-auto" title="Android"></i>
                    &nbsp;<i class="fa fa-linux tooltip-auto" title="Linux"></i>
                    &nbsp;<i class="fa fa-apple tooltip-auto" title="OS-X + iOS"></i>
                    <span class="lighter">&nbsp; |&nbsp;</span>
                    &nbsp;<i class="fa fa-list-alt tooltip-auto" title="All Browsers"></i>
                    <span class="lighter">&nbsp; |&nbsp;</span>
                    &nbsp;<i class="fa fa-globe tooltip-auto" title="Worldwide Coverage"></i>
                    <span class="lighter">&nbsp; |&nbsp;</span>
                    &nbsp;<i class="fa fa-clock-o tooltip-auto" title="Distributed evenly"></i>
                </td>
            </tr>
            <tr>
                <th>Campaign:</th>
                <td>Domain Redirect</td>
                <td colspan="2">Keyword-Targeted + Blacklist</td>
            </tr>

            <?php if ($model == 'CPC'): ?>
            <tr>
                <th valign="top">Preview:</th>
                <td colspan="3">
                    <div id="ppcpreview_title">Running Shoes‎</div>
                    <div id="ppcpreview_text1">Free Ship &amp; Perfect Fit Guarantee</div>
                    <div id="ppcpreview_text2">On All Of Your Favorite Shoes!</div>
                    <div id="ppcpreview_dispurl">www.roadrunnersports.com</div>
                </td>
            </tr>
            <?php endif; ?>
        </table>
    </div>

    <!-- main chart -->
    <div class="col-md-7 no-padding" style="padding-bottom:4px;border-bottom:1px solid #eee">
        <div class="chart-legend">
            <i class="fa fa-square" style="color:#0c7930"> Traffic</i> &nbsp;
            <i class="fa fa-square" style="color:#49af61"> Conversions</i>
        </div>
        <div style="overflow:hidden;">
            <div id="main-chart" class="chart" style="height:<?php echo ($model == 'CPC') ? 314 : 244; ?>px;"></div>
        </div>
    </div>
    <div class="clearfix"><p>&nbsp;</p></div>

    <script>
    mainChartData = [
        { "period": "2014-10-18", "traffic": 12345, "conversions": (123*250) },
        { "period": "2014-10-19", "traffic": 0, "conversions": 0 },
        { "period": "2014-10-20", "traffic": 12345, "conversions": (123*250) },
        { "period": "2014-10-21", "traffic": 18524, "conversions": (14*250) },
        { "period": "2014-10-22", "traffic": 12345, "conversions": (123*250) },
        { "period": "2014-10-23", "traffic": 19384, "conversions": (193*250) },
        { "period": "2014-10-24", "traffic": 12345, "conversions": (123*250) },
        { "period": "2014-10-25", "traffic": 12266, "conversions": (122*250) }
    ];
    </script>