<?php
// header
$bodyclass = 'form campaign-create';
require_once('_header.php');

// sidebar
// require_once('_sidebar.php');

// mode
$model = (@$_GET['type']) ? $_GET['type'] : 'pop';
$mode = (@$_GET['mode']) ? $_GET['mode'] : 'new';

// top tabs
if (@$_GET['first'] == 1) {
    echo '<div id="header" class="container-max" style="margin-top:-6px"></div>';
} else {
    $active_tab = $mode;
    require_once('_tabs.php');
}

?>

<div class="container" id="maincontent">

<?php if ($mode=='edit'): ?>
    <h3 class="no-margin-top green">Edit <span class="model-text"><?php echo ucwords($model);?></span> Campaign</h3>
    <hr>
<?php else: ?>
    <?php if (@$_GET['first']==1): ?>
    <h1 class="no-margin green">One last step...</h1>
    <h3 class="no-margin-top">Fill out the form below to <strong>create your first campaign</strong>...</h3>
    <div style="height:20px" class="clearfix"></div><ul class="nav nav-tabs">
        <li id="pop-tab" class="model-tab<?php if ($model == 'pop') echo ' active"'; ?>"><a href="create-campaign.php?model=ppv&amp;first=1" onclick="switchCampaignType('pop', true);">Pop/Interstitial</a></li>
        <li id="domain-redirect-tab" class="model-tab<?php if ($model == 'domain-redirect') echo ' active"'; ?>"><a href="create-campaign.php?model=ppc&amp;first=1" onclick="switchCampaignType('domain-redirect', true);">Domain Redirect</a></li>
        <li id="search-tab" class="model-tab<?php if ($model == 'search') echo ' active"'; ?>"><a href="create-campaign.php?model=ppc&amp;first=1" onclick="switchCampaignType('search', true);">Search Campaign</a></li>
    </ul><div style="height:30px" class="clearfix"></div>
    <?php else: ?>
    <ul class="nav nav-tabs">
        <li id="pop-tab" class="model-tab<?php if ($model == 'pop') echo ' active"'; ?>"><a href="create-campaign.php?model=pop" onclick="switchCampaignType('pop', false);">Pop/Interstitial</a></li>
        <li id="domain-redirect-tab" class="model-tab<?php if ($model == 'domain-redirect') echo ' active"'; ?>"><a href="create-campaign.php?model=domain-redirect" onclick="switchCampaignType('domain-redirect', false);">Domain Redirect</a></li>
        <li id="search-tab" class="model-tab<?php if ($model == 'search') echo ' active"'; ?>"><a href="create-campaign.php?model=search" onclick="switchCampaignType('search', false);">Search Campaign</a></li>
    </ul><div style="height:30px" class="clearfix"></div>
    <h3 class="no-margin-top green">New <span class="model-text"><?php echo ucwords($model);?></span> Campaign</h3>
    <hr>
    <?php endif; ?>
<?php endif; ?>

<form class="form-horizontal" role="form" method="post" action="<?php echo (@$_GET['first'] == 1) ? './create-campaign-billing.php?first=1' : './campaign-group.php'; ?>">

    <!-- campaign -->
   <h4 class="col-sm-2 text-right no-margin-top green simple">Campaign Information</h4>
   <div class="clearfix"></div>

    <div class="form-group">
        <label for="inputName" class="col-sm-2 control-label">Group name:</label>
        <div id="group-new" style="display:none">
            <div class="col-sm-3"><input type="text" class="form-control" id="inputGroup" placeholder="Group name"></div>
            <div class="col-sm-2 light" style="padding-top:7px"><i class="fa fa-list-ul"></i> &nbsp;<a href="#" class="light" onclick="$('#group-new').toggle(); $('#group-list').toggle(); return false;">Show list</a></div>
        </div>
        <div id="group-list">
            <div class="col-sm-3"><select id="inputGroupList" class="form-control selectpicker" data-live-search="true">
                <option selected>Campaign Group Name</option>
                <option>Campaign Group 2</option>
                <option>...</option>
            </select></div>
            <div class="col-sm-2 light" style="padding-top:7px"><i class="fa fa-plus-square"></i> &nbsp;<a href="#" class="light" onclick="$('#group-new').toggle(); $('#group-list').toggle(); $('#inputGroup').focus(); return false;">Create new</a></div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Targeting Type:</label>
        <div class="col-sm-3"><select class="form-control selectpicker" onchange="if($(this).val() === 'kw') { $('#keywords-targeted').slideDown(); } else { $('#keywords-targeted').slideUp(); }">
            <option value="ron">Run-of-Network (more traffic)</option>
            <option value="kw"<?php if ($mode=='edit'): ?> selected<?php endif;?>>Keyword-Targeted</option>
        </select></div>
        <div class="col-sm-1"><a href="#" onclick="$(this).parent().parent().next().slideToggle(); return false" class="help-icon"><i class="fa fa-question-circle"></i></a></div>
    </div>
    <div class="below-help" style="display:none">
        <div class="col-sm-offset-2 col-sm-5 help">With a Run-of-Network (RoN) campaign you will likely get more traffic to your page, whereas with a Keyword-Targeted campaign you can be more precise.</div>
        <div class="clearfix"></div>
    </div>


    <div class="col-sm-offset-2 col-sm-5"><hr style="margin:20px 0 10px 0"></div>
    <div class="clearfix"></div>

    <div class="form-group">
        <label for="inputName" class="col-sm-2 control-label">Campaign name:</label>
        <div class="col-sm-3"><input type="text" class="form-control" id="inputName" placeholder="Campaign name"></div>
    </div>
    <div class="form-group">
        <label for="inputBudget" class="col-sm-2 control-label">Daily Budget:</label>
        <div class="col-sm-2 price-wrapper"><input class="pull-left form-control price" id="inputBudget" value="100.00"></div>
        <div class="col-sm-1"><a href="#" onclick="$(this).parent().parent().next().slideToggle(); return false" class="help-icon"><i class="fa fa-question-circle"></i></a></div>
    </div>
    <div class="below-help" style="display:none">
        <div class="col-sm-offset-2 col-sm-5 help">Your daily budget is the average amount you're comfortable spending each day on this campaign.
            <span class="ppc-item"><br> As traffic fluctuates, you may spend up to 20% more than your daily budget on a given day.</span>
        </div>
        <div class="clearfix"></div>
    </div>


    <div class="form-group search-item model-item"<?php if ($model != 'search'): ?>style="display:none"<?php endif; ?>>
        <label for="inputPPC" class="col-sm-2 control-label">CPC:</label>
        <div class="col-sm-2 price-wrapper"><input class="pull-left form-control price" id="inputCPC" value="0.01"></div>
    </div>

    <div class="form-group domain-redirect-item model-item"<?php if ($model != 'domain-redirect'): ?>style="display:none"<?php endif; ?>>
        <label for="inputCPR" class="col-sm-2 control-label">CPR:</label>
        <div class="col-sm-2 price-wrapper"><input class="pull-left form-control price" id="inputCPR" value="0.001"></div>
    </div>

    <div class="form-group pop-item model-item"<?php if ($model != 'pop'): ?>style="display:none"<?php endif; ?>>
        <label for="inputCPV" class="col-sm-2 control-label">CPV:</label>
        <div class="col-sm-2 price-wrapper"><input class="pull-left form-control price" id="inputCPV" value="0.001"></div>
    </div>


    <div class="col-sm-offset-2 col-sm-5"><hr style="margin:20px 0 10px 0"></div>
    <div class="clearfix"></div>

    <div class="clearfix" style="height:30px"></div>
    <h4 class="col-sm-2 text-right no-margin-top green simple">Campaign Offer</h4>
    <div class="clearfix"></div>

    <!-- landing page -->
    <div class="pop-item model-item"<?php if ($model != 'pop') echo ' style="display:none"'; ?>>
        <div class="clearfix"></div>
        <div class="form-group">
            <label for="inputUrl" class="col-sm-2 control-label">Destination URL:</label>
            <div class="col-sm-5"><input type="text" class="form-control" id="inputUrl" placeholder="http://"></div>
            <div class="col-sm-1"><a href="#" onclick="$(this).parent().parent().next().slideToggle(); return false" class="help-icon"><i class="fa fa-question-circle"></i></a></div>
        </div>
        <div class="below-help" style="display:none">
            <div class="col-sm-offset-2 col-sm-5 help">You can use <code>%%source%%</code> or <code>%%keyword%%</code> in your URLs and we'll pass the Source ID and Keyword to your destination URL for easy tracking.</div>
            <div class="clearfix"></div>
        </div>
    </div>

    <!-- landing page -->
    <div class="domain-redirect-item model-item"<?php if ($model != 'domain-redirect') echo ' style="display:none"'; ?>>
        <div class="clearfix"></div>
        <div class="form-group">
            <label for="inputUrl" class="col-sm-2 control-label">Destination URL:</label>
            <div class="col-sm-5"><input type="text" class="form-control" id="inputUrl" placeholder="http://"></div>
            <div class="col-sm-1"><a href="#" onclick="$(this).parent().parent().next().slideToggle(); return false" class="help-icon"><i class="fa fa-question-circle"></i></a></div>
        </div>
        <div class="below-help" style="display:none">
            <div class="col-sm-offset-2 col-sm-5 help">You can use <code>%%source%%</code> or <code>%%keyword%%</code> in your URLs and we'll pass the Source ID and Keyword to your destination URL for easy tracking.</div>
            <div class="clearfix"></div>
        </div>
    </div>

    <!-- ad -->
    <div class="search-item model-item"<?php if ($model != 'search') echo ' style="display:none"'; ?>>
        <div id="ppcpreview" class="col-sm-offset-6 col-sm-3 hidden-sm hidden-xs">
            <strong>Preview:</strong>
            <div id="ppcpreview_wrapper">
                <div id="ppcpreview_title">Headline</div>
                <div id="ppcpreview_text1">Description line 1</div>
                <div id="ppcpreview_text2">Description line 2</div>
                <div id="ppcpreview_dispurl">Display URL</div>
            </div>
        </div>
        <div class="form-group">
            <label for="inputTitle" class="col-sm-2 control-label">Headline:</label>
            <div class="col-sm-3"><input type="text" class="form-control" id="inputTitle" placeholder="Headline" onkeyup="$('#ppcpreview_title').html((this.value=='')?this.placeholder:this.value);"></div>
        </div>
        <div class="form-group">
            <label for="inputText1" class="col-sm-2 control-label">Description:</label>
            <div class="col-sm-4"><input type="text" class="form-control" id="inputText1" placeholder="Description line 1" onkeyup="$('#ppcpreview_text1').html((this.value=='')?this.placeholder:this.value);"></div>
        </div>
        <div class="form-group" style="margin-top:5px !important">
            <label for="inputText2" class="col-sm-2 control-label"></label>
            <div class="col-sm-4"><input type="text" class="form-control" id="inputText2" placeholder="Description line 2" onkeyup="$('#ppcpreview_text2').html((this.value=='')?this.placeholder:this.value);"></div>
        </div>
        <div class="form-group">
            <label for="inputDispURL" class="col-sm-2 control-label">Display URL:</label>
            <div class="col-sm-3"><input type="text" class="form-control" id="inputDispURL" placeholder="Display URL" onkeyup="$('#ppcpreview_dispurl').html((this.value=='')?this.placeholder:this.value);"></div>
        </div>
        <div class="form-group">
            <label for="inputUrl" class="col-sm-2 control-label">Destination URL:</label>
            <div class="col-sm-5"><input type="text" class="form-control" id="inputUrl" placeholder="http://"></div>
            <div class="col-sm-1"><a href="#" onclick="$(this).parent().parent().next().slideToggle(); return false" class="help-icon"><i class="fa fa-question-circle"></i></a></div>
        </div>
        <div class="below-help" style="display:none">
            <div class="col-sm-offset-2 col-sm-5 help">You can use <code>%%source%%</code> or <code>%%keyword%%</code> in your URLs and we'll pass the Source ID and Keyword to your destination URL for easy tracking.</div>
            <div class="clearfix"></div>
        </div>
    </div>

    <!-- conversion tracking -->
    <div class="col-sm-offset-2 col-sm-5"><hr style="margin:20px 0 10px 0"></div>
    <div class="clearfix"></div>

    <div class="clearfix" style="height:30px"></div>
    <h4 class="col-sm-2 text-right no-margin-top green simple">Conversion Tracking</h4>
    <div class="clearfix"></div>

    <div class="form-group">
        <label for="inputConv" class="col-sm-2 control-label">Track Conversions?</label>
        <div class="col-sm-5" style="padding-top:6px;padding-bottom:0">
            <input type="radio" id="conversion-off" checked="checked" name="track-conversions" value="yes" onclick="$('#conversion-tracking-wrapper').slideUp()"><label for="conversion-off" class="normal">&nbsp; Not at this time</label>
            &nbsp; &nbsp; &nbsp;
            <input type="radio" id="conversion-on" name="track-conversions" value="no" onclick="$('#conversion-tracking-wrapper').slideDown()"><label for="conversion-on" class="normal">&nbsp; Hellz, Yeah!</label>
        </div>
    </div>

    <div id="conversion-tracking-wrapper">

        <div id="conversion-tracking-info" class="col-sm-offset-2 col-sm-5"><div>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </div></div>
        <div class="clearfix"></div>

        <div class="form-group" id="conversion-tracking">
            <label for="inputConv" class="col-sm-2 control-label">Avg. Conversion Value:</label>
            <div class="col-sm-2 price-wrapper"><input class="pull-left form-control price" id="inputConv" value="0"></div>
        </div>

        <div class="form-group">
            <label for="inputUrl" class="col-sm-2 control-label">Type:</label>
            <div class="col-sm-5" style="padding-top:6px;padding-bottom:0">
                <input type="radio" id="conversion-pixel" checked="checked" name="conversion-type" value="pixel" onclick="conversionCode('pxl')"><label for="conversion-pixel" class="normal">&nbsp; Pixel</label>
                &nbsp; &nbsp; &nbsp;
                <input type="radio" id="conversion-postback" name="conversion-type" value="postback" onclick="conversionCode('pb')"><label for="conversion-postback" class="normal">&nbsp; Postback</label>
            </div>
        </div>
        <div class="form-group" id="conversion-pxl-code">
            <label for="inputUrl" class="col-sm-2 control-label">JS Pixel:</label>
            <div class="col-sm-5"><textarea id="conversion-pxl-input" class="form-control" rows="4" disabled>&lt;script type="text/javascript"&gt;
    (function(u){ try{var pxl=new Image(); pxl.src=u;}catch(er){}
    })('http://beacon.trkjmp.net/?type=js&cid=0420d6-05d97eb-746182c-e41019-70b03a&value=0.00');
    &lt;/script&gt;</textarea>
            </div>
        </div>
        <div class="form-group" id="conversion-pb-code" style="display:none">
            <label for="inputUrl" class="col-sm-2 control-label">Postback URL:</label>
            <div class="col-sm-5">
                <input id="conversion-pb-input" type="text" class="form-control" disabled value="http://beacon.trkjmp.net/?type=pb&cid=0420d6-05d97eb-746182c-e41019-70b03a&value=0.00">
            </div>
        </div>
    </div>

    <div class="col-sm-offset-2 col-sm-5"><hr style="margin:20px 0 10px 0"></div>
    <div class="clearfix"></div>


    <!-- targeting -->
    <div class="clearfix" style="height:40px"></div>
    <h4 class="col-sm-2 text-right no-margin-top green simple">Campaign Targeting</h4>
    <div class="clearfix"></div>

    <!-- keywords targeting -->
    <div class="form-group form-group-tight" id="keywords-targeted"<?php if ($mode=='new'): ?> style="display:none"<?php endif;?>>
        <label class="col-sm-2 control-label">Keywords:</label>
        <div class="col-md-3">
            <h5><i class="fa fa-check green"></i> Active Keywords</h5>
            <textarea name="keywords" class="form-control" rows="15" placeholder="Enter the keywords you want to target"></textarea>
        </div>
        <div class="col-md-2 no-padding-left">
            <h5><i class="fa fa-ban red"></i> Blocked Keywords</h5>
            <textarea name="keywords-blocked" class="form-control" rows="15" placeholder="Enter keywords to block"></textarea>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-offset-2 col-sm-5"><hr style="margin:20px 0 10px 0"></div>
    </div>

    <!-- source targeting -->
    <div class="form-group">
        <label class="col-sm-2 control-label">Targeting Mode:</label>
        <div class="col-sm-3"><select class="form-control selectpicker" onchange="if($(this).val() === 'target-list') { switchSourceIdMode('whitelist'); } else { switchSourceIdMode('blacklist'); }">
            <option value="block-list" selected>Blacklist (all available sources except blacklisted)</option>
            <option value="target-list">Whitelist (specific sources only)</option>
        </select></div>
        <div class="col-sm-1"><a href="#" onclick="$(this).parent().parent().next().slideToggle(); return false" class="help-icon"><i class="fa fa-question-circle"></i></a></div>
    </div>
    <div class="below-help" style="display:none">
        <div class="col-sm-offset-2 col-sm-5 help">In <strong>Blacklist</strong> mode, you will receive traffic from all relevant sources - as long as they are not in your blacklist. Contrary, with <strong>Whitelist</strong> mode, you will only receive traffic from specific sources.</div>
        <div class="clearfix"></div>
    </div>

    <div class="form-group" id="source-targeted">
        <label class="col-sm-2 control-label">Manage Black List:</label>
        <div class="col-md-5">
            <textarea id="sourceid-text" name="source-blocked" class="form-control blacklist" rows="10" placeholder=""></textarea>
        </div>
    </div>

    <!-- campaign targeting -->
    <div class="clearfix" style="height:40px"></div>
    <h4 class="col-sm-2 text-right no-margin-top green simple">Audience Targeting</h4>
    <div class="clearfix"></div>
    <div class="form-group form-group-tight">
        <label for="inputGeo" class="col-sm-2 control-label">Countries:</label>
        <div class="col-sm-2 no-padding-left"><a href="#" onclick="$(this).parent().parent().next().slideToggle(); return false" class="btn" style="margin-top:1px"><span id="geoIndicator">All Countries</span> <i class="fa fa-pencil"></i></a></div>
    </div>
    <div class="form-groups below-help" style="display:none">
        <div class="col-sm-offset-2 col-sm-8 expanded">
            <ul class="list-inline list-unstyled" id="geo-checkboxes-groups">
                <li><input type="checkbox" name="geoselect" checked onclick="geoSelect()" value="tier1" id="geo-group-tier1"> <label for="geo-group-tier1" class="simple">Tier 1 Countries</label></li>
                <li><input type="checkbox" name="geoselect" checked onclick="geoSelect();" value="tier2" id="geo-group-tier2"> <label for="geo-group-tier2" class="simple">Tier 2 Countries</label></li>
                <li><input type="checkbox" name="geoselect" checked onclick="geoSelect();" value="tier3" id="geo-group-tier3"> <label for="geo-group-tier3" class="simple">Tier 3 Countries</label></li>
                <li style="border-left:1px solid #ccc;margin-left:10px;padding-left:10px"><a href="#" onclick="geoSelect('all'); return false"> Select/Deselect all</a></li>
            </ul>
            <hr>
            <ul class="list-unstyled list-quad" id="geo-checkboxes"><?php
            include('_countries.php');
            foreach ($FLATDB_GEOS_CODES as $code=>$name){
                echo '<li><input type="checkbox" name="countries[]" checked value="'.$code.'" onchange="geoSingleChange($(this))" id="geo-'.$code.'"><label for="geo-'.$code.'" class="simple">'.$name.'</label></li>';
            }
            ?><div class="clearfix"></div></ul>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="col-sm-offset-2 col-sm-5"><hr style="margin:10px 0"></div>
    <div class="clearfix"></div>

    <!-- devices -->
    <div class="form-group form-group-tight">
        <label for="inputDevices" class="col-sm-2 control-label">Devices:</label>
        <div class="col-sm-8" style="padding-top:8px">
            <ul class="list-inline list-unstyled">
                <li><input type="checkbox" name="devices[]" checked value="DesktopsLaptops" id="inputDesktops"> <label for="inputDesktops" class="simple">Desktops &amp; Laptops</label></li>
                <li><input type="checkbox" name="devices[]" checked value="Tablets" id="inputTablets"> <label for="inputTablets" class="simple">Tablets</label></li>
                <li><input type="checkbox" name="devices[]" checked value="Phones" id="inputPhones"> <label for="inputPhones" class="simple">Smartphones</label></li>
            </ul>
        </div>
    </div>
    <!-- platforms -->
    <div class="form-group form-group-tight">
        <label for="inputDevices" class="col-sm-2 control-label">Platforms:</label>
        <div class="col-sm-8" style="padding-top:8px">
            <ul class="list-inline list-unstyled">
                <li><input type="checkbox" name="platforms[]" checked value="windows" id="inputTabletWindows"> <label for="inputTabletWindows" class="simple">Windows</li>
                <li><input type="checkbox" name="platforms[]" checked value="macos" id="inputMacOS"> <label for="inputMacOS" class="simple">OS-X (Macintosh)</label></li>
                <li><input type="checkbox" name="platforms[]" checked value="linux" id="inputLinux"> <label for="inputLinux" class="simple">Linux</label></li>
                <li><input type="checkbox" name="platforms[]" checked value="android" id="inputTabletAndroid"> <label for="inputTabletAndroid" class="simple">Android</label></li>
                <li><input type="checkbox" name="platforms[]" checked value="ios" id="inputTabletIOS"> <label for="inputTabletIOS" class="simple">iOS (iPhones/iPads/iPods)</li>
            </ul>
        </div>
    </div>

    <!-- browsers -->
    <div class="form-group form-group-tight">
        <label for="inputDevices" class="col-sm-2 control-label">Browsers:</label>
        <div class="col-sm-8" style="padding-top:8px">
            <ul class="list-inline list-unstyled">
                <li><input type="checkbox" name="desktop_clients[]" checked value="chrome" id="inputChrome"> <label for="inputChrome" class="simple">Chrome</label></li>
                <li><input type="checkbox" name="desktop_clients[]" checked value="msie" id="inputMSIE"> <label for="inputMSIE" class="simple">Internet Explorer</label></li>
                <li><input type="checkbox" name="desktop_clients[]" checked value="firefox" id="inputFirefox"> <label for="inputFirefox" class="simple">Firefox</label></li>
                <li><input type="checkbox" name="desktop_clients[]" checked value="safari" id="inputSafari"> <label for="inputSafari" class="simple">Safari</label></li>
                <li><input type="checkbox" name="desktop_clients[]" checked value="opera" id="inputOther"> <label for="inputOther" class="simple">Other</label></li>
            </ul>
        </div>
    </div>



    <!-- capping -->
    <div class="clearfix" style="height:30px"></div>
    <h4 class="col-sm-2 text-right no-margin-top green simple">Campaign Delivery</h4>
    <div class="clearfix"></div>
    <div class="form-group">
        <label for="inputFrequency" class="col-sm-2 control-label">Exposure:</label>
        <div class="col-sm-2 frequency-wrapper"><input class="no-margin-left pull-left form-control price" id="inputFrequency" value="3"></div>
    </div>

    <div class="form-group">
        <label for="inputDelivery" class="col-sm-2 control-label">Distribution:</label>
        <div class="col-sm-8" style="padding-top:8px">
            <input class="pull-left" style="margin:2px 5px 0 0 !important" type="radio" name="delivery" checked value="evenly"> Evenly throughout the day<br>
            <div class="clearfix" style="height:5px"></div>
            <input class="pull-left" style="margin:2px 5px 0 0 !important" type="radio" name="delivery" value="quick"> As soon as possible
        </div>
    </div>

    <div class="form-group">
        <label for="inputParting" class="col-sm-2 control-label">Day Parting:</label>
        <div class="col-sm-8 no-padding-left">
            <div class="no-padding-left">&nbsp;<a href="#" onclick="$(this).parent().parent().next().slideToggle(); return false" class="btn" style="margin-top:1px"><span id="partingIndicator">24 hours, 7 days/week</span> <i class="fa fa-pencil"></i></a></div>
        </div>
        <div class="col-sm-offset-2 col-sm-8" style="display:none">
            <?php $days  = array('mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'); ?>
            <p style="margin:5px 0" class="text-sm light"><strong style="color:red">*</strong> Time zone for all dates and times: (GMT-05:00) Eastern Time.</p>
            <table id="parting" class="table-bordered">
                <tr>
                    <td id="xcell"></td>
                    <?php foreach ($days as $day): ?>
                    <th><input type="checkbox" value="<?php echo $day; ?>" class="parting-rowcol" id="parting-<?php echo $day; ?>" checked> <label for="parting-<?php echo $day; ?>"><?php echo ucwords($day); ?></label></th>
                    <?php endforeach; ?>
                </tr>
                <?php for ($i=0; $i < 24; $i++):?>
                <tr>
                    <th><input type="checkbox" value="<?php echo $i; ?>" class="parting-rowcol" id="parting-<?php echo $i; ?>" checked> <label for="parting-<?php echo $i; ?>"><?php echo date('h:00 A', strtotime(str_replace('*', $i, date('Y-m-d *:00:00')))); ?></label></th>
                    <?php foreach ($days as $day): ?>
                    <td class="parting-on parting-<?php echo $day; ?> parting-<?php echo $i; ?>"><input type="checkbox" name="parting[]" class="parting-<?php echo $day; ?> parting-<?php echo $i; ?>" value="<?php echo $day; ?>-<?php echo $i; ?>" checked></td>
                    <?php endforeach; ?>
                </tr>
                <?php endfor; ?>
            </table>
            <p style="margin:5px 0" class="text-sm light"><strong style="color:red">*</strong> Time zone for all dates and times: (GMT-05:00) Eastern Time.</p>
        </div>
    </div>

    <div class="form-group">
        <label for="inputDelivery" class="col-sm-2 control-label">Flight Time:</label>
        <div class="col-sm-8" style="padding-top:8px">
            <input class="pull-left" style="margin:2px 5px 0 0 !important" type="radio" name="stop" checked value="never" onchange="$('#selectdate').hide()"> Run forever (until I stop it)<br>
            <div class="clearfix" style="height:5px"></div>
            <input class="pull-left" style="margin:2px 5px 0 0 !important" type="radio" name="stop" value="date" onchange="$('#selectdate').show()"> End on specific date
            <span id="selectdate" style="display:none">&nbsp;
            <select name="day" class="form-controls selectpicker select-sm" data-size="5">
                <option>Day</option>
                <?php for ($i=1; $i<32 ; $i++) {
                    echo "<option>$i</option>";
                } ?>
            </select>
            <select name="month" class="form-controls selectpicker select-sm" data-size="5">
                <option>Month</option>
                <option>January</option>
                <option>February</option>
                <option>March</option>
                <option>April</option>
                <option>May</option>
                <option>June</option>
                <option>July</option>
                <option>August</option>
                <option>September</option>
                <option>October</option>
                <option>November</option>
                <option>December</option>
            </select>
            <select name="year" class="form-controls selectpicker select-sm" data-size="5">
                <option>Year</option>
                <?php for ($i=date('Y'); $i<date('Y')+11 ; $i++) {
                    echo "<option>$i</option>";
                } ?>
            </select>
            </span>
        </div>
    </div>
    <!-- <h4 class="no-margin-top green">Campaign Delivery</h4> -->


    <div class="clearfix" style="height:15px"></div>
    <hr>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-green">Send Campaign for Approval <i class="fa fa-angle-right"></i></button>
        </div>
    </div>
</form>

<?php
require_once('_footer.php');
?>
