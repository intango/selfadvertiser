<?php
// header
$bodyclass = 'campaign';
require_once('_header.php');

// sidebar
require_once('_sidebar.php');

// top tabs
$active_tab = 'campaign';
require_once('_tabs.php');
?>



<div class="container" id="maincontent">

    <?php require_once('_campaign.php'); ?>

    <!-- tabs -->
    <ul class="nav nav-tabs">
        <li><a href="campaign-kw.php?type=<?php echo @$_GET['type']; ?>">Keywords</a></li>
        <li class="active"><a href="campaign-kw-sources.php?type=<?php echo @$_GET['type']; ?>" onclick="return false;">Sources</a></li>
        <li><a href="campaign-audience.php?type=<?php echo @$_GET['type']; ?>">Audience</a></li>
    </ul>

    <?php
    $show_block = true;
    require_once('_campaign-toolbar.php');
    ?>

    <!-- main table -->
    <table class="table table-bordered table-hover" id="listitems">
        <thead>
            <tr class="active">
                <th class="check"><input type="checkbox"></th>
                <th class="status hidden-xs"><i class="fa fa-circle status-gray"></i></th>
                <th class="text-left">Source ID</th>
                <th>Views</th>
                <?php if ($model == 'CPC'): ?>
                <th>Clicks</th>
                <th>CTR</th>
                <?php endif; ?>
                <th>Cost</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="check"><input type="checkbox"></td>
                <td class="status hidden-xs"><div class="dropdown inline">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-circle status-green"></i></a>
                    <ul class="dropdown-menu arrow-left" style="width:180px">
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-remove status-red"></i> Block source</a></li>
                        <li><hr style="margin:5px 0"></li>
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-plus status-green"></i> Use in new campaign</a></li>
                    </ul>
                </div></td>
                <td class="text-left">1236871 (broad mode campaign)</td>
                <td>12,345</td>
                <?php if ($model == 'CPC'): ?>
                <td>1,234</td>
                <td>10.00%</td>
                <td>$123.40</td>
                <?php else: ?>
                <td>$123.45</td>
                <?php endif ;?>
            </tr>
            <tr>
                <td class="check"><input type="checkbox"></td>
                <td class="status hidden-xs"><div class="dropdown inline">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-circle status-gray"></i></a>
                    <ul class="dropdown-menu arrow-left" style="width:180px">
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-check status-green"></i> Unblock source</a></li>
                        <li><hr style="margin:5px 0"></li>
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-plus status-green"></i> Use in new campaign</a></li>
                    </ul>
                </div></td>
                <td class="text-left">1236871 (broad mode campaign)</td>
                <td>12,345</td>
                <?php if ($model == 'CPC'): ?>
                <td>1,234</td>
                <td>10.00%</td>
                <td>$123.40</td>
                <?php else: ?>
                <td>$123.45</td>
                <?php endif ;?>
            </tr>
            <tr>
                <td class="check"><input type="checkbox"></td>
                <td class="status hidden-xs"><div class="dropdown inline">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-circle status-green"></i></a>
                    <ul class="dropdown-menu arrow-left" style="width:180px">
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-remove status-red"></i> Un-target source</a></li>
                        <li><hr style="margin:5px 0"></li>
                        <li><a href="#" onclick="return rusure(this)"><i class="fa fa-plus status-green"></i> Use in new campaign</a></li>
                    </ul>
                </div></td>
                <td class="text-left">1236871 (target mode campaign)</td>
                <td>12,345</td>
                <?php if ($model == 'CPC'): ?>
                <td>1,234</td>
                <td>10.00%</td>
                <td>$123.40</td>
                <?php else: ?>
                <td>$123.45</td>
                <?php endif ;?>
            </tr>
        </tbody>
    </table>


<?php
require_once('_pagination.php');
require_once('_footer.php');
?>