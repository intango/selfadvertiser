<?php
// header
$bodyclass = 'form campaign-create';
require_once('_header.php');

// sidebar
// require_once('_sidebar.php');

// top tabs
$active_tab = 'new';
require_once('_tabs.php');
?>

<div class="container" id="maincontent">

<h1 class="no-margin green">You're almost done...</h1>
<h3 class="no-margin-top">To get your campaign up and running, you need to deposit some dinero...</h3>
<hr>
<form class="form-horizontal" role="form">

    <div class="col-md-6 no-padding-left">
        <a href="#" class="btn btn-green btn-lg" style="zoom:1.5" data-toggle="modal" data-target="#ppModal"><i class="fa fa-paypal"></i>&nbsp; Deposit using PayPall&nbsp; </a>
        &nbsp;
        <a href="#" class="btn btn-silver btn-lg" style="zoom:1.5"><i class="fa fa-credit-card"></i>&nbsp; Deposit using Credit Card&nbsp; </a>
    </div>
    <div class="clearfix"></div>
    &nbsp;

    <hr>
    <h4 class="simple"><a href="./home.php">I'll do it later (return to home)</a></h4>
</form>

<style type="text/css" media="screen">

</style>

<?php
require_once('modal-paypal.php');
require_once('_footer.php');
?>