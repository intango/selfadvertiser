<?php
// header
$bodyclass = 'notice campaign-group';
require_once('_header.php');

// sidebar
require_once('_sidebar.php');

// top tabs
$group_view = true;
$active_tab = 'all';
require_once('_tabs.php');
?>


<div class="container" id="maincontent">

    <!-- main chart -->
    <div id="mobile-daterage"></div>

    <!-- mini charts -->
    <?php require_once('_minicharts.php'); ?>

    <!-- toolbar -->
    <div id="toolbar">
        <div class="dropdown main-button inline">
            <a href="#" class="dropdown-toggle btn btn-green" data-toggle="dropdown"><i class="fa fa-plus"></i> New Campaign <i class="fa fa-caret-down"></i></a>
            <ul class="dropdown-menu arrow-left">
                <li><a href="create-campaign.php?model=pop"><i class="fa fa-external-link"></i>&nbsp; Pop/Interstitial</a></li>
                <li><a href="create-campaign.php?model=domain-redirect"><i class="fa fa-bolt" style="margin:0 3px 0 2px"></i>&nbsp; Domain Redirect</a></li>
                <li><a href="create-campaign.php?model=search"><i class="fa fa-search"></i>&nbsp; Search Campaign</a></li>
            </ul>
        </div>
        <a href="#" class="btn btn-silver"><i class="fa fa-arrow-circle-down" style="font-size:1em;margin-left:-3px;"></i> &nbsp;Download&nbsp;</a>
    </div>

    <!-- main table -->
    <table class="table table-bordered table-hover" id="listitems">
        <thead>
            <tr class="active">
                <th class="text-left">Group</th>
                <th class="text-left">Campaigns</th>
                <th>Budget</th>
                <th>Cost</th>
                <th>Traffic</th>
                <th>Conv.</th>
                <th>Conv. $</th>
                <th>Conv. $</th>
                <th>$/Visit</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-left"><a href="campaign-group.php" class="settings">Campaign Group</a></td>
                <td class="text-left"><a href="campaign-group.php" class="green">2 running</a>, <a href="campaign-group.php" class="orange">1 pending approval</a></td>
                <td>$25.00<span class="hidden-xs">/day</span></td>
                <td>$246.85</td>
                <td>12,345</td>
                <td>1,234</td>
                <td>10.00%</td>
                <td>$123.40</td>
                <td>$0.10</td>
            </tr>
            <tr>
                <td class="text-left"><a href="campaign-group.php" class="settings">Campaign Group 2</a></td>
                <td class="text-left"><a href="campaign-group.php" class="red">3 out of budget campaigns (1 pending)</a></td>
                <td>$10.00<span class="hidden-xs">/day</span></td>
                <td>$100.31</td>
                <td>321</td>
                <td>-</td>
                <td>-</td>
                <td>$0.00</td>
                <td>$0.00</td>
            </tr>
        </tbody>
    </table>

<?php
require_once('_pagination.php');
require_once('_footer.php');
?>