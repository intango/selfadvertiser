<!-- PayPal Modal -->
<div class="modal fade" id="ppModal" tabindex="-1" role="dialog" aria-labelledby="ppModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sms">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="ppModalLabel">Deposit funds</h4>
            </div>
            <form onsubmit="go2paypal()">
            <div class="modal-body">
                <p>Enter the deposit size you would like to fund your account with ($10 mimium), and click on the Checkout button below.</p>
                <hr>
                <p><strong>Deposit amount:</strong></p>
                <div class="pull-left price-wrapper price-wrapper-lg" style="width:150px"><input class="pull-left form-control price" id="inputDeposit" value="100.00"></div>
                <div class="pull-left" style="padding-top:14px">&nbsp; <strong class="red">*</strong> 18% VAT will be added for advertisers based in <strong>ISRAEL</strong>.</div>
                <div class="clearfix"></div>
                &nbsp;
            </div>
            <div class="modal-footer">
                <img src="./media/img/loading.gif" id="pploader">
                <button type="submit" id="ppbtn" onclick="go2paypal();" class="smoooth btn btn-primary btn-lg btn-paypal"><i class="fa fa-paypal"></i>Checkout using <strong>PayPal</strong> &nbsp;<i class="fa fa-caret-right"></i></button>
            </div>
            </form>
        </div>
    </div>
</div>
