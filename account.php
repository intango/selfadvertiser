<?php
// header
$bodyclass = 'account';
require_once('_header.php');
?>

<!-- header w/ tabs -->
<div id="header" class="container-max">
    <h2>Account Settings</h2>
    <div class="clearfix"></div>
    <ul class="nav nav-tabs">
        <li class="active"><a href="account.php">Advertiser Profile</a></li>
        <li><a href="billing.php">Billing Panel</a></li>
    </ul>
</div>

<div class="container" id="maincontent">

<form class="form-horizontal" role="form">
    <!-- warning -->
    <p class="alert alert-danger" role="alert"><i class="fa icon fa-warning"></i> Error messages goes here, along with marked fields!</p>

    <!-- account info -->
    <div class="col-md-4">

        <h4 class="green simple" style="margin-bottom:20px">Contact information</h4>

        <div class="form-group">
            <label for="inputFName">First &amp; Last Name:</label>
            <div>
                <div class="col-md-5 no-padding-left">
                    <input type="text" class="form-control" id="inputFName" placeholder="First name" value="Ran">
                </div>
                <div class="col-md-7 no-padding">
                    <input type="text" class="form-control" id="inputLName" placeholder="Last name" value="Aroussi">
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <label for="inputEmail">Email:</label>
            <input type="email" class="form-control" id="inputEmail" placeholder="Email" value="ran@aroussi.com">
        </div>

        &nbsp;<hr>

        <h4 class="green simple" style="margin-bottom:20px">Login information</h4>

        <div class="form-group">
            <label for="inputUserName">New Password: <span class="gray simple">(leave blank to un-change)</span></label>
            <div>
                <div class="col-md-5 no-padding-left">
                    <input type="password" class="form-control" id="inputUserName" placeholder="new password">
                </div>
                <div class="col-md-7 no-padding">
                    <input type="password" class="form-control" id="inputUserName" placeholder="re-type password">
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="form-group">
            <label for="inputUserName">Username:</label>
            <input type="text" disabled class="form-control" id="inputUserName" value="username">
            <div class="small gray"><strong class="red">*</strong> To change this, please contact us directly</div>
        </div>

        &nbsp;<hr>
    </div>



    <div class="col-sm-4 col-md-offset-1">
        <h4 class="green simple" style="margin-bottom:20px">Billing information</h4>
        <div class="form-group<?php if (@$_GET['invoicee']==1): ?> flash<?php endif; ?>">
            <label for="inputCompany">Invoicee: <span class="gray simple">(name on invoice, optional)</span></label>
            <input type="text" class="form-control" id="inputCompany" placeholder="Company Name" value="RevenueHits">
        </div>
        <div class="form-group">
            <label for="inputPhone">Phone No:</label>
            <input type="phone" class="form-control" id="inputPhone" placeholder="Phone No." value="+972-544-607608">
        </div>
        <div class="form-group">
            <label for="inputAddress">Billing Address:</label>
            <textarea name="address" id="inputAddress" class="form-control" rows="4" placeholder="Address">5 Haplada St.
Or-Yehuda, Israel</textarea>
            <div class="small gray"><strong class="red">*</strong> Please include Street address, state and country</div>
        </div>
    </div>

    <div class="clearfix"></div>



    <div class="form-group">
        <div class="col-md-12">
            <button type="submit" class="btn btn-green">Update Details <i class="fa fa-angle-right"></i></button>
        </div>
    </div>

    <div class="clearfix"></div>
    <p>&nbsp;</p>

</form>

<?php
require_once('_footer.php');
?>