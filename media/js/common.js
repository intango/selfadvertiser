function sideMenuSelected(selected) {
    alert("You've selected: "+ selected);
}

function statsOffline(msg) {
    $('#statserror').remove();
    $(document).ready(function(){
        $('.chart-wrapper').hide();
        $('#main-chart').hide();
        $('.chart-legend').hide();
        $('#maincontent table#mainForm\\:listitems').hide();
        $('#maincontent #mainForm\\:reportsPanel').hide();
        $('#maincontent').prepend('<div id="statserror" style="background:#e74734;color:#fff;text-align:center;padding:20px 10px">'+msg+'</div>');
    });
}

function switchSourceIdMode(mode) {
    mode = (mode == 'whitelist') ? 'whitelist' : 'blacklist';
    $('#sourceid-mode').val(mode);
    $('#sources-tabs li').removeClass('active').addClass('inactive');
    $('#sources-tabs li#sourceid-'+mode).addClass('active').removeClass('inactive');

    $('#sourceid-text').removeClass('blacklist').removeClass('whitelist');
    $('#sourceid-text').addClass(mode);


    if (mode == 'whitelist') {
        $('#sourceid-text').attr("placeholder", "Enter Source IDs to Target");
        $('#source-targeted label').html('White List:<br><span style="color:#808080;font-weight:normal">(required)</span>');
    } else {
        $('#sourceid-text').attr("placeholder", "Enter Source IDs to Blacklist");
        $('#source-targeted label').html('Black List:<br><span style="color:#808080;font-weight:normal">(optional)</span>');
    }
}

function switchModel(model, first) {
     try {
        if (event.preventDefault) {
            event.preventDefault();
        } else {
            event.returnValue = false;
        }
    } catch(er) {}

    model = (model.toLowerCase() == 'ppc') ? 'ppc' : 'ppv';
    try {
        if (first === true) {
            history.pushState(null, null, '?type='+model+'&first=1');
        } else {
            history.pushState(null, null, '?type='+model);
        }
    } catch(er){}

    $('#ppv-tab').removeClass('active');
    $('.ppv-item').hide();
    $('#ppc-tab').removeClass('active');
    $('.ppc-item').hide();
    $('#'+model+'-tab').addClass('active');
    $('.'+model+'-item').show();

    $('.model-cap').html(model.toUpperCase());
}

function ucwords(str) {
    return (str + '').replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, function($1) {
        return $1.toUpperCase();
    });
}
function switchCampaignType(model, first) {
     try {
        if (event.preventDefault) {
            event.preventDefault();
        } else {
            event.returnValue = false;
        }
    } catch(er) {}

    // model = (model.toLowerCase() == 'ppc') ? 'ppc' : 'ppv';
    try {
        if (first === true) {
            history.pushState(null, null, '?model='+model+'&first=1');
        } else {
            history.pushState(null, null, '?model='+model);
        }
    } catch(er){}

    $('.model-tab').removeClass('active');
    $('.model-item').hide();

    $('#'+model+'-tab').addClass('active');
    $('.'+model+'-item').show();

    $('.model-text').html(ucwords(model.replace('-', ' ')));
}

Number.prototype.formatNumber = function(c, d, t) {
    var n = this;
        c = isNaN(c = Math.abs(c)) ? 2 : c;
        d = d === undefined ? "." : d;
        t = t === undefined ? "," : t;
    var s = n < 0 ? "-" : "";
    var i = parseInt(n = Math.abs(+n || 0).toFixed(c), 0) + "";
    var j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

jQuery.fn.putCursorAtEnd = function() {
    return this.each(function() {
        $(this).focus();
        // If this function exists...
        if (this.setSelectionRange) {
            // ... then use it (Doesn't work in IE)
            // Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
            var len = $(this).val().length * 2;
            this.setSelectionRange(len, len);
        } else {
            // ... otherwise replace the contents with itself
            // (Doesn't work in Google Chrome)
            $(this).val($(this).val());
        }
        // Scroll to the bottom, in case we're in a tall textarea
        // (Necessary for Firefox and Google Chrome)
        this.scrollTop = 999999;
    });
};

// alerts & confirms
alert = swal;
badAlert = function(msg){
    swal({
        title: "Error",
        text: msg,
        type: "error"
    });
};
goodAlert = function(msg){
    swal({
        title: "Success",
        text: msg,
        type: "success"
    });
};
function rusure(ele) {
    var go2link = ele.href || '#';
    var winName = ele.target || null;
    swal({
        title: "Are you sure?",
        text: "Please confirm that you want to proceed...",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#5cb85c",
        confirmButtonText: "Yes, Please Continue!",
        closeOnConfirm: true
    }, function(){
        if (winName) {
            window.open(go2link, winName);
        } else {
            window.location.href = go2link;
        }
    });
    return false;
}
// alerts & confirms

function go2paypal() {

    // handle button
    $('#pploader').show();
    $('#ppbtn').hide();
    setTimeout(function(){
        $('#pploader').hide();
        $('#ppbtn').show();
    }, 8*1000);

    // redirect
    setTimeout(function(){
        window.location.href = "./billing.php?after=1";
    }, 500);
}

function initTooltips() {
    $(".tooltip-auto").tooltip({placement : 'auto'});
    $(".tooltip-top").tooltip({placement : 'top'});
    $(".tooltip-right").tooltip({placement : 'right'});
    $(".tooltip-bottom").tooltip({placement : 'bottom'});
    $(".tooltip-left").tooltip({ placement : 'left'});
}

function initEditableInputs() {
    // shrink editable elements by input
    $('input.rename').each(function(){
        $(this).css('width', (($(this).val().length+1)*8)+'px');
    });
    $('input.rename').keypress(function(){
        $(this).css('width', (($(this).val().length+1)*8)+'px');
    });
    $('input.rename.h1').each(function(){
        $(this).css('width', (($(this).val().length+1)*18)+'px');
    });
    $('input.rename.h1').keypress(function(){
        $(this).css('width', (($(this).val().length+1)*18)+'px');
    });
    $('input.rename.h2').each(function(){
        $(this).css('width', (($(this).val().length+1)*14.2)+'px');
    });
    $('input.rename.h2').keypress(function(){
        $(this).css('width', (($(this).val().length+1)*14.2)+'px');
    });
    $('input.rename.h3').each(function(){
        $(this).css('width', (($(this).val().length+1)*11)+'px');
    });
    $('input.rename.h3').keypress(function(){
        $(this).css('width', (($(this).val().length+1)*11)+'px');
    });

    // editable elements
    var rename_focus = false;
    $('.inline-rename').click(function(){
        $(this).addClass('active');
        $(this).find('input.rename').prop('disabled', false);
        $(this).find('input.rename').focus();
        if (!rename_focus) {
            $(this).find('input.rename').select();
            rename_focus = true;
        }
    });
    $('input.rename').focusout(function(){
        rename_focus = false;
        $(this).closest('.inline-rename').removeClass('active');
        alert('Send data to backend for saving');
    });
}

function initCheckboxes() {
    var list_checkboxes_checked = false;
    $('table#listitems thead input:checkbox').change(function(){
        if (!list_checkboxes_checked) {
            $('table#listitems tbody input:checkbox:enabled').prop('checked', true);
            list_checkboxes_checked = true;
            $('#toolbar_actions').removeClass('disabled');
        } else {
            $('table#listitems tbody input:checkbox:enabled').prop('checked', false);
            list_checkboxes_checked = false;
            $('#toolbar_actions').addClass('disabled');
        }
    });

    $('table#listitems tbody input:checkbox').change(function(){
        var items_checked = $('table#listitems tbody input:checkbox:checked').length;
        if (items_checked > 0) {
            $('#toolbar_actions').removeClass('disabled');
            if (items_checked === 1) {
                $('#toolbar_edit').removeClass('disabled');
            } else {
                $('#toolbar_edit').addClass('disabled');
            }
        } else {
            $('#toolbar_edit').addClass('disabled');
            $('#toolbar_actions').addClass('disabled');
        }
        if (items_checked === $('table#listitems tbody input:checkbox').length) {
            $('table#listitems thead input:checkbox').prop('checked', true);
            list_checkboxes_checked = true;
        } else {
            $('table#listitems thead input:checkbox').prop('checked', false);
            list_checkboxes_checked = false;
        }
    });
}

function initDatePicker() {
    var dateRanges = {
        'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
        '2 Days Ago': [moment().subtract('days', 2), moment().subtract('days', 2)],
        'Last 7 Days': [moment().subtract('days', 8), moment().subtract('days', 1)],
        'Last 30 Days': [moment().subtract('days', 29), moment().subtract('days', 1)],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
    };


    $('#reportrange').daterangepicker({
        // startDate: moment().subtract('days', 29),
        // endDate: moment(),
        startDate: moment(viewRange.start),
        endDate: moment(viewRange.end),
        dateLimit: {
            days: 365
        },
        showDropdowns: true,
        ranges: dateRanges,
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-sm btn-primary',
        cancelClass: 'btn-sm',
        format: 'MM/DD/YYYY',
        separator: ' to '

    }, function(start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

        var url = '?';
        // add your url params here...
        url += '&start='+start.format('YYYY-MM-DD')+'&end='+end.format('YYYY-MM-DD');
        url = url.replace('?&', '?');
        window.location.href = url;
    });
}

var chartTimeout, activityChart, mainChartData = mainChartData || [];
function initMainChart() {
    activityChart = {
        'element': 'main-chart',
        'data': mainChartData,
        'xkey': 'period',
        'ykeys': ['conversions', 'traffic'],
        'labels': ['Conversions', 'Traffic'],
        'lineColors': ['#8bcc9f', '#0c7930'],
        'smooth': false,
        'grid': false,
        'resize': true,
        'lineWidth': 0,
        'pointSize': 0,
        'fillOpacity': .75,
        'behaveLikeLine': true,
        'hideHover': 'auto',
        'hoverCallback': function (index, options, content) {
            var row = options.data[index];
            var html  = '<div class="morris-legend">';
                html += '<div class="morris-hover-row-label" style="text-align:center;text-transform:uppercase;color:#000;border-bottom:1px solid #ccc;padding-bottom:2px;margin-bottom:6px;">'+moment(new Date(row.period)).format("MMM D, YYYY")+'</div>';
                html += '<div class="morris-hover-point" style="color:#0c7930"><span class="pull-left">Traffic:</span><span style="font-weight:bold;" class="pull-right">'+(row.traffic*1).formatNumber(0)+'</span></div><div class="clearfix"></div>';
                html += '<div class="morris-hover-point" style="color:#49af61"><span class="pull-left">Conv.:</span><span style="font-weight:bold;" class="pull-right"> &nbsp; &nbsp;'+(row.conversions/250).formatNumber(0)+'</span></div><div class="clearfix"></div>';
                html += '<div class="clearfix"></div></div>';
            return html;
        },
        'yLabelFormat': function(d) { return ''; },
        'xLabelFormat': function(d) {
            return moment(new Date(d)).format("MMM D");
        }
    };
    if (activityChart.data.length < 28) { activityChart.xLabels = 'day'; }
    new Morris.Area(activityChart);

    // handle resize
    /*
    $(window).resize(function() {
        try {
            clearTimeout(chartTimeout);
            chartTimeout = setTimeout(function() {
                $('#main-chart').html('');
                new Morris.Area(activityChart);
            }, 100);
        } catch(err){}

        try {
            $('#sidebar-container').css('height', ($(window).height()-40-$('#sidebar-container').position().top)+'px !important');
        } catch(err) {}
    });
    */
}

var ctData = ctData || [], tcData = tcData || [];
function initMiniCharts(){
    // DEVICE BARS
    var i = 0;
    $('.bar-wrapper .bar').each(function(){
        var table = $(this).closest('.barchart');
        var item = $(table).find('th')[i].innerHTML || ""; i++;
        var datatxt = $(this).attr('rel-data')+'%';
        $(this).css('width', datatxt );
        $(this).parent().attr('title', item +': '+ datatxt);
    });
    $('.barchart [data-toggle="tooltip"]').tooltip();


    // CAMPAIGN TYPE
    ctData = ctData || [];
    tcMiniChart = {
        'element': 'minichart-campaign-type',
        'resize': true,
        'data': ctData,
        'colors': ['#0c7930', '#9cdbaf', '#4ea66a', '#1c833d', '#83ca99'],
        'formatter': function (y, data) { return y+'%'; }
    };
    new Morris.Donut(tcMiniChart);


    // CONVERSIONS / TRAFFIC CHART
    tcData = tcData || [];
    tcMiniChart = {
        'element': 'minichart-traffic-conversions',
        'smooth': false,
        'data': tcData,
        'xkey': 'period',
        'ykeys': ['conversions', 'traffic'],
        'labels': ['Conversions', 'Traffic'],
        'lineColors': ['#8bcc9f', '#0c7930'],
        'grid': false,
        'axes': false,
        'resize': true,
        'lineWidth': 0,
        'pointSize': 0,
        'fillOpacity': .75,
        'behaveLikeLine': true,
        'hideHover': 'auto',

        'hoverCallback': function (index, options, content) {
            var row = options.data[index];
            var html  = '<div class="morris-legend">';
                html += '<div class="morris-hover-row-label" style="text-align:center;text-transform:uppercase;color:#000;border-bottom:1px solid #ccc;padding-bottom:2px;margin-bottom:6px;">'+moment(new Date(row.period)).format("MMM D, YYYY")+'</div>';
                html += '<div class="morris-hover-point" style="color:#0c7930"><span class="pull-left">Traffic:</span><span style="font-weight:bold;" class="pull-right">'+(row.traffic*1).formatNumber(0)+'</span></div><div class="clearfix"></div>';
                html += '<div class="morris-hover-point" style="color:#49af61"><span class="pull-left">Conv.:</span><span style="font-weight:bold;" class="pull-right"> &nbsp; &nbsp;'+(row.conversions/250).formatNumber(0)+'</span></div><div class="clearfix"></div>';
                html += '<div class="clearfix"></div></div>';
            return html;
        },

        'yLabelFormat': function(d) { return ''; },
        'xLabelFormat': function(d) {
            return moment(new Date(d)).format("MMM D");
        }
    };
    if (tcMiniChart.data.length < 28) { tcMiniChart.xLabels = 'day'; }
    new Morris.Area(tcMiniChart);
}

var inlineListVisible = false;
function toggleInlineList() {
    if (inlineListVisible) {
        $('#manage-inline-lists').slideUp();
        $('#show-inline-lists').removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
        inlineListVisible = false;
    } else {
        $('#manage-inline-lists').slideDown();
        $('#show-inline-lists').removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
        inlineListVisible = true;
    }
    return false;
}

var budgetTimer;
function budgetify(offer_id, element) {
    $(element).val($(element).val().replace('$', '')).removeClass('blend').TouchSpin({
        verticalbuttons: true,
        min: 5,
        max: 50000,
        step: 1,
        decimals: 2,
        verticalupclass: 'fa fa-plus',
        verticaldownclass: 'fa fa-minus',
        prefix: '$'
    });

    $(element).change(function(){
        clearTimeout(budgetTimer);
        budgetTimer = setTimeout(function() {
            alert('Updating budget of offerID: '+offer_id+' to: $'+$(element).val());
        }, 500);
    });
}
var cpvTimer;
function cpvify(offer_id, element) {
    $(element).val($(element).val().replace('$', '')).removeClass('blend').TouchSpin({
        verticalbuttons: true,
        min: 0.01,
        max: 50,
        step: 0.01,
        decimals: 2,
        verticalupclass: 'fa fa-plus',
        verticaldownclass: 'fa fa-minus',
        prefix: '$'
    });

    $(element).change(function(){
        clearTimeout(cpvTimer);
        cpvTimer = setTimeout(function() {
            alert('Updating CPV of offerID: '+offer_id+' to: $'+$(element).val());
        }, 500);
    });
}


(function(sidebar, undefined) {
    sidebar.pinned = true;

    sidebar.remember = function(){
        var now     = new Date(),
            expiry  = new Date(now.setTime(now.getTime()+86400*365*1000)).toGMTString();
            document.cookie = 'sidebar='+sidebar.pinned+';expires=' + expiry + ';path=/';
    };

    sidebar.recall = function(){
        if (document.cookie.indexOf('sidebar=') !== -1) {
            sidebar.pinned = (document.cookie.split('sidebar=')[1].split(';')[0] === 'true');
        }
    };

    sidebar.pin = function(){
        sidebar.pinned=(!sidebar.pinned);
        sidebar.remember();
        $('#sidebar-pin').toggleClass('pinned');
        return false;
    };

    sidebar.open = function() {
        $('#campaign-list').addClass('on');
        $('#sidebar-container').attr('style', 'height:'+ ($(window).height()-40-$('#sidebar-container').position().top)+'px !important');
        $('body').removeClass('nosidebar').addClass('sidebar');

        try{ setTimeout(function(){
            $('#campaign-list').addClass('done');

            $('#main-chart').html('');
            new Morris.Area(activityChart);
            sidebar.bindOpened();

        }, 450); } catch(er){}
    };

    sidebar.close = function() {
        $('#campaign-list').removeClass('on').removeClass('done');
        $('body').removeClass('sidebar').addClass('nosidebar');

        try{ setTimeout(function(){
            $('#main-chart').html('');
            new Morris.Area(activityChart);
            sidebar.bindClosed();
        }, 450); } catch(er){}
    };

    sidebar.unbind = function() {
        $('#campaign-list').unbind("mouseenter");
        $('#campaign-list #closedbar').unbind("mouseleave");
    };

    sidebar.bindOpened = function() {
        sidebar.unbind();
        $('#campaign-list').mouseleave(function(){
            if (!sidebar.pinned) {
                sidebar.close();
            }
        });
    };

    sidebar.bindClosed = function() {
        if ($('#campaign-list').length===0) {
            return;
        }
        sidebar.recall();
        if (sidebar.pinned) {
            $('#sidebar-pin').addClass('pinned');
            $('#campaign-list').addClass('on');
            $('#sidebar-container').attr('style', 'height:'+ ($(window).height()-40-$('#sidebar-container').position().top)+'px !important');
            $('body').removeClass('nosidebar').addClass('sidebar');
            sidebar.bindOpened();

            setTimeout(function(){
                $('#campaign-list').addClass('done');
            }, 450);
            return;
        }
        sidebar.unbind();
        $('#campaign-list #closedbar').mouseenter(function(){
            sidebar.open();
        });

        $('body.sidebar').addClass('transition5');
        $('body.nosidebar').addClass('transition5');
        $('#campaign-list').addClass('transition5');
        $('#campaign-list #closedbar').addClass('transition2');
    };

}(window.sidebar = window.sidebar || {}));


// geo list
var activeGeoGroups = {
    "all": true,
    "tier1": true,
    "tier2": true,
    "tier3": true,
    "tier1_geos": ['CA', 'GB', 'US'],
    "tier2_geos": ['AT', 'AU', 'BE', 'CH', 'DE', 'DK', 'FI', 'FR', 'IE', 'IT', 'LU', 'NL', 'NO', 'SE']
};


function geoSingleChange(ele) {
    $('#geo-checkboxes-groups input:checkbox').prop('checked', false);
    $('#geo-checkboxes input').parent().removeClass('fade');

    if ($('#geo-checkboxes input:checked').length === $('#geo-checkboxes input').length) {
        $('#geo-checkboxes-groups input:checkbox').prop('checked', true);
        $('#geoIndicator').html('All Countries');
    } else {
        $('#geoIndicator').html($('#geo-checkboxes input:checked').length + ' Countries');
        $('#geo-checkboxes input:not(:checked)').parent().addClass('fade');
    }
}

function geoFixLegend(){

    $('#geoIndicator').html('');
    if (activeGeoGroups.tier1) {
        $('#geoIndicator').html('Tier 1 ');
    }
    if (activeGeoGroups.tier2) {
        $('#geoIndicator').html($('#geoIndicator').html() + '+ Tier 2 ');
    }
    if (activeGeoGroups.tier3) {
        $('#geoIndicator').html($('#geoIndicator').html() + '+ Tier 3 ');
    }
    $('#geoIndicator').html($('#geoIndicator').html() + 'Countries');

    if ($('#geo-checkboxes input:checked').length === $('#geo-checkboxes input').length) {
        $('#geoIndicator').html('All Countries');
    }
    if ($('#geo-checkboxes input:checked').length === 0) {
        $('#geoIndicator').html('<span class="red"><i class="fa fa-warning icon"></i>&nbsp; No Countries Selected</span>');
    }

    if ($('#geoIndicator').html()[0] == '+') {
        $('#geoIndicator').html($('#geoIndicator').html().replace('+ ', ''));
    }

    $('#geo-checkboxes input').parent().removeClass('fade');
    $('#geo-checkboxes input:not(:checked)').parent().addClass('fade');
}

function geoSelect(group) {
    // all or nothing
    if (group == 'all') {
        $('#geo-checkboxes input:checkbox').prop('checked', !activeGeoGroups.all);
        $('#geo-checkboxes-groups input:checkbox').prop('checked', !activeGeoGroups.all);
        activeGeoGroups.all   = !activeGeoGroups.all;
        activeGeoGroups.tier1 = !activeGeoGroups.all;
        activeGeoGroups.tier2 = !activeGeoGroups.all;
        activeGeoGroups.tier3 = !activeGeoGroups.all;
        if (!activeGeoGroups.all) {
            $('#geo-checkboxes li').addClass('fade');
        } else {
            $('#geo-checkboxes li').removeClass('fade');
        }
        geoFixLegend();
        return;
    }

    // selective
    $('#geo-checkboxes-groups input:checkbox').each(function(){
        activeGeoGroups[$(this).val()] = $(this).prop('checked');
    });

    // tier 1
    $('#geo-checkboxes input:checkbox').each(function(){
        if ($.inArray($(this).val(), activeGeoGroups.tier1_geos) !== -1) {
            $(this).prop('checked', activeGeoGroups.tier1);
            // geoMarkAsFaded($(this));
        }
    });
    $('#geo-checkboxes input:checkbox').each(function(){
        if ($.inArray($(this).val(), activeGeoGroups.tier2_geos) !== -1) {
            $(this).prop('checked', activeGeoGroups.tier2);
            // geoMarkAsFaded($(this));
        }
    });
    $('#geo-checkboxes input:checkbox').each(function(){
        if ($.inArray($(this).val(), activeGeoGroups.tier1_geos) === -1 && $.inArray($(this).val(), activeGeoGroups.tier2_geos) === -1) {
            $(this).prop('checked', activeGeoGroups.tier3);
            // geoMarkAsFaded($(this));
        }
    });

    if (group == 'none') {
        $('#geo-checkboxes-groups input:checkbox').prop('checked', false);
        allGeosSelected = false;
    }

    geoFixLegend();
}



function partingIndicator() {
    if ($('#parting td input').length === $('#parting td input:checked').length) {
        $('#partingIndicator').html('24 hours, 7 days/week');
    } else {
        $('#partingIndicator').html('Custom settings');
    }
}

function conversionCode(method){
    $('#conversion-pxl-code').hide();
    $('#conversion-pb-code').hide();
    $('#conversion-'+method+'-code').show();
}
// everythig is loaded - show the body
jQuery(document).ready(function($) {

    // search groups
    setTimeout(function(){
        $('#sidebar-container .bootstrap-select button.selectpicker span.filter-option').html('<i class="fa fa-search pull-right"></i> Search Campaigns');
        $('#sidebar-container .bootstrap-select .bs-searchbox input').attr('placeholder', 'Search Campaigns');
        $('#sidebar-container .bootstrap-select button').attr('title', '');
        $('#sidebar-container .bootstrap-select').show();
        $('#sidebar-container .bootstrap-select').css('visibility', 'visible');

        $('#inputGroupsList.selectpicker').on('change', function(){
            var selected = $(this).find("option:selected").val();
            sideMenuSelected(selected);
        });
    }, 1000);

    setTimeout(function(){
        $('#inputConv').change(function(){
            var pb = $('#conversion-pb-input').val().split('value=')[0]+'value='+$(this).val();
            $('#conversion-pb-input').val(pb);

            var pxl = $('#conversion-pxl-input').val().split('value=');
            var newpxl = pxl[0]+'value='+$(this).val()+'\''+pxl[1].split('\'')[1];
            $('#conversion-pxl-input').val(newpxl);
        });
    }, 1000);

    try {
        $('#parting input.parting-rowcol').click(function(){
            // event.preventDefault();
            $('#parting input.'+this.id).prop("checked", !$('#parting input.'+this.id).prop("checked"));

            $('#parting td.'+this.id).removeClass('parting-on');
            if ($('#parting input.'+this.id).prop("checked")) {
                $('#parting td.'+this.id).addClass('parting-on');
            }

            partingIndicator();
        });
        $('#parting td input').click(function(){
            // return;
            $(this).parent().removeClass('parting-on');
            if ($(this).prop("checked")) {
                $(this).parent().addClass('parting-on');
            }
            partingIndicator();
        });
    } catch(er){}


    try {
        switchSourceIdMode('blacklist');
    } catch(er){}

    try {
        // disabling interactive sidebar
        // sidebar.bindClosed();
        $('#sidebar-container').attr('style', 'height:'+ ($(window).height()-40-$('#sidebar-container').position().top)+'px !important');
    } catch(er){}


    // page loaded - show it
    setTimeout(function(){
        $('body').removeClass('preload');
        try{ NProgress.done(); }catch(er){}
    }, 500);


    $('label.tree-toggler').click(function () {
        $(this).parent().children('ul.tree').toggle(300);
    });

    try {
        $('#inputBudget').TouchSpin({
            verticalbuttons: true,
            min: 5,
            max: 50000,
            step: 1,
            decimals: 2,
            verticalupclass: 'fa fa-plus',
            verticaldownclass: 'fa fa-minus',
            prefix: '$',
            postfix: '/ day'
        });
        $('#inputCPV').TouchSpin({
            verticalbuttons: true,
            min: 0.001,
            max: 5,
            step: 0.001,
            decimals: 3,
            verticalupclass: 'fa fa-plus',
            verticaldownclass: 'fa fa-minus',
            prefix: '$',
            postfix: '/ view'
        });
        $('#inputCPR').TouchSpin({
            verticalbuttons: true,
            min: 0.001,
            max: 5,
            step: 0.001,
            decimals: 3,
            verticalupclass: 'fa fa-plus',
            verticaldownclass: 'fa fa-minus',
            prefix: '$',
            postfix: '/ view'
        });
        $('#inputCPC').TouchSpin({
            verticalbuttons: true,
            min: 0.01,
            max: 5,
            step: 0.01,
            decimals: 2,
            verticalupclass: 'fa fa-plus',
            verticaldownclass: 'fa fa-minus',
            prefix: '$',
            postfix: '/ click'
        });
        $('#inputConv').TouchSpin({
            verticalbuttons: true,
            min: 0.01,
            max: 5000,
            step: 1,
            decimals: 2,
            verticalupclass: 'fa fa-plus',
            verticaldownclass: 'fa fa-minus',
            prefix: '$',
            postfix: '/ conversion'
        });
        $('#inputFrequency').TouchSpin({
            verticalbuttons: true,
            min: 1,
            max: 24,
            step: 1,
            decimals: 0,
            verticalupclass: 'fa fa-plus',
            verticaldownclass: 'fa fa-minus',
            postfix: '/ day / user'
        });
    } catch(er){}

    try {
        $('#inputDeposit').TouchSpin({
            verticalbuttons: true,
            min: 10,
            max: 5000,
            step: 1,
            decimals: 2,
            verticalupclass: 'fa fa-plus',
            verticaldownclass: 'fa fa-minus',
            prefix: '$'
        });
    } catch(er){}

    $('#toggle_removed').click(function(){
        $(this).toggleClass('on');
        $('.hidden-row').toggle();
        return false;
    });

    $('#faq .faq').click(function() {
        $(this).next().slideToggle();
    });

    try { initTooltips(); } catch(er) {}
    try { initEditableInputs(); } catch(er) {}
    try { initCheckboxes(); } catch(er) {}
    try { initDatePicker(); } catch(er) {}
    try { initMainChart(); } catch(er) {}
    try { initMiniCharts(); } catch(er) {}

    $('a').click(function(){
        $(this).blur();
    });

});

// highlight stuff
$(window).load(function(){
    setTimeout(function(){
        $('.flash').addClass('start-flash');
        setTimeout(function(){
            $('.flash').removeClass('start-flash').addClass('finish-flash');
        }, 500);
    }, 250);
});
