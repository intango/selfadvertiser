function showPasswordForm() {
    $('#login-form').hide();
    $('#password-form').fadeIn();
    return false;
}
function showLoginForm() {
    $('#login-form').fadeIn();
    $('#password-form').hide();
    return false;
}
function doLogin() {
    if ($('#username').val()==='' || $('#password').val()==='') {
        $('#login-msg').removeClass('alert-success').addClass('alert-danger').html('Missing required fields').fadeIn('fast');
        return false;
    } else {
        $('body').addClass('preload');
        return true;
    }
}
function doPassword() {
    if ($('#pusername').val()==='') {
        $('#password-msg').removeClass('alert-success').addClass('alert-danger').html('Please fill out your username').fadeIn('fast');
    } else {
        // generate password via ajax and email to user
        $('#login-msg').removeClass('alert-danger').addClass('alert-success').html('A newly generated password was sent to your@email.com').fadeIn('fast');
        showLoginForm();
    }
    return false;
}