(function(){
    var browser = function() {
        var n = navigator.userAgent.toLowerCase();
        var b = {
            webkit: /webkit/.test(n),
            mozilla: (/mozilla/.test(n)) && (!/(compatible|webkit)/.test(n)),
            chrome: /chrome/.test(n),
            msie: ((/msie/.test(n) || /trident/.test(n) || !! window.MSStream) && !/opera/.test(n)),
            firefox: /firefox/.test(n),
            safari: (/safari/.test(n) && !(/chrome/.test(n))),
            opera: /opera/.test(n),
            mobile: /mobile|ipod|iphone|ipad|ios|android|webos|blackberry/i.test(n),
            tablet: /tablet/i.test(n)
        };
        b.version = (b.safari) ? (n.match(/.+(?:ri)[\/: ]([\d.]+)/) || [])[1] : (n.match(/.+(?:ox|me|ra|ie)[\/: ]([\d.]+)/) || [])[1];
        try { b.version = parseFloat(b.version); } catch(er) {}
        return b;
    }();

    if ((browser.msie && browser.version < 10) || (browser.safari && browser.version < 7) ||
        (browser.firefox && browser.version < 30) || (browser.chrome && browser.version < 30)) {
            try {
                window.location.replace('./unsupported/');
            } catch(er) {
                window.location.href = './unsupported/';
            }
    }
}());