<?php
// header
$bodyclass = 'form campaign-group-create';
require_once('_header.php');

// sidebar
// require_once('_sidebar.php');

// top tabs
$active_tab = 'none';
require_once('_tabs.php');
?>

<div class="container" id="maincontent">

<form class="form-horizontal" role="form" action="create-campaign.php">
    <!-- info -->

    <?php if (@$_GET['createcampaign']==1): ?>
    <h4 class="col-sm-2 text-right no-margin-top green simple">Select Campaign Group</h4>
    <div class="clearfix"></div>

    <div class="form-group">
        <label for="inputName" class="col-sm-2 control-label">Name:</label>
        <div class="col-sm-3"><select class="form-control selectpicker" data-live-search="true">
            <option selected>Campaign Group Name</option>
            <option>Campaign Group 2</option>
            <option>...</option>
        </select></div>
    </div>
    <!-- <div class="clearfix" style="height:40px"></div> -->
    <hr>
    <h4 class="col-sm-2 text-right no-margin-top green simple">...or Create a New Group</h4>
    <?php else: ?>
    <h4 class="col-sm-2 text-right no-margin-top green simple">Group Information</h4>
    <?php endif; ?>

    <div class="clearfix"></div>


    <div class="form-group">
        <label for="inputName" class="col-sm-2 control-label">Name:</label>
        <div class="col-sm-3"><input type="email" class="form-control" id="inputName" placeholder="Group name"></div>
    </div>

    <div class="form-group">
        <label for="inputDescription" class="col-sm-2 control-label">Description:</label>
        <div class="col-sm-5"><input type="text" class="form-control" id="inputDescription" placeholder="(optional)"></div>
    </div>


    <div class="clearfix" style="height:15px"></div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-green"><?php if (@$_GET['createcampaign']==1): ?>Proceed to Create Campaign<?php else: ?>Save &amp; Create the First Campaign<?php endif;?> <i class="fa fa-angle-right"></i></button>
        </div>
    </div>
</form>

<?php
require_once('_footer.php');
?>