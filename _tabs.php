<!-- header w/ tabs -->
<div id="header" class="container-max">
    <!-- Content area -->

    <?php if ($active_tab !== 'new' && $active_tab !== 'edit' && $active_tab !== 'none'): ?>
    <div id="reportrange" class="date-range-selector pull-right">
        <span id="prettyrange"></span><i class="fa fa-calendar"></i> &nbsp;<i class="fa fa-caret-down"></i>
    </div>
    <?php endif; ?>

    <?php if ($active_tab == 'none'): ?>
        <?php if (@$_GET['createcampaign']==1): ?>
        <h2>Create a New Campaign</h2>
        <?php else: ?>
        <h2>New Campaign Group</h2>
        <?php endif; ?>
        <div class="clearfix" style="height:10px"></div>
    <?php elseif (@$group_view): ?>
        <h2>My Campaigns</h2>
        <div class="clearfix" style="height:10px"></div>
    <?php else: ?>

        <?php if ($active_tab == 'new' || $active_tab == 'edit'): ?>
        <h2>Campaign Group Name</h2>
        <?php else: ?>
        <form role="rename-campaign" class="inline-rename" onsubmit="return false">
            <input type="hidden" name="campaign_id" value="123">
            <input type="text" name="campaign_name" value="Campaign Group Name" class="h2 rename" disabled>
        </form>
        <?php endif; ?>
        <div class="clearfix"></div>

        <ul class="nav nav-tabs">
            <li<?php if ($active_tab == 'running'):?> class="active"<?php endif;?>><a href="campaign-group.php">Running<span class="hidden-xs"> Campaigns</span></a></li>
            <li<?php if ($active_tab == 'all'):?> class="active"<?php endif;?>><a href="campaign-group.php" onclick="return false;">All <span class="hidden-xs"> Campaigns</span></a></li>
            <?php if ($active_tab == 'campaign'):?><li class="active"><a href="#" onclick="return false;"><stronsg>Active Campaign</stsrong></a></li><?php endif; ?>
            <?php if ($active_tab == 'new'):?><li class="active"><a href="#" onclick="return false;"><stronsg>New Campaign (draft)</stsrong></a></li><?php endif; ?>
            <?php if ($active_tab == 'edit'):?><li class="active"><a href="#" onclick="return false;"><stronsg>Active Campaign (editing)</stsrong></a></li><?php endif; ?>
        </ul>
    <?php endif; ?>
</div>