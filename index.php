<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="robots" content="noindex">
    <link rel="shortcut icon" href="./favicon.ico">

    <title>Admin Console - Dashboard</title>

    <!-- Bootstrap + Custom CSS -->
    <link href="./media/css/bootstrap.css" rel="stylesheet">
    <link href="./media/css/bootstrap-theme.css" rel="stylesheet">
    <link href="./media/css/font-awesome.css" rel="stylesheet">
    <link href="./media/css/font-proxima.css" rel="stylesheet">
    <link href="./media/css/icons.css" rel="stylesheet">
    <link href="./media/css/layout.css" rel="stylesheet">

    <!-- Bootstrap + jQuery + Custom JS -->
    <script src="./media/js/jquery.min.js" ></script>
    <script src="./media/js/bootstrap.min.js"></script>

    <!-- SweetAlert - alert replacer -->
    <link href="./media/css/sweet-alert.css" rel="stylesheet">
    <script src="./media/js/sweet-alert.min.js" ></script>

    <!-- NProgress - Progress Bar -->
    <link href="./media/css/nprogress.css" rel="stylesheet">
    <script src="./media/js/nprogress.js" ></script>

    <!-- main Javascripts -->
    <script src="./media/js/common.js"></script>
    <script src="./media/js/login.js"></script>
    <script src="./media/js/browsercheck.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="./media/js/html5shiv.min.js"></script>
        <script src="./media/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="login">
<script> NProgress.start(); </script>
<div id="heros" class="col-md-2"></div>

<div id="wrap"><div class="container">
    <div class="navbar-brand"></div>
    <a href="signup.php" class="pull-right btn btn-sm btn-success" id="create-account" style="margin-top:15px;padding:3px 10px">Don't have an account? Click here to sign up &raquo;</a>

    <form class="form-signin" action="./home.php" method="post" onsubmit="return doLogin();" id="login-form">
        <h2 class="form-signin-heading">Advertiser Login</h2>
        <p id="login-msg" class="alert" style="display:none"></p>
        <input type="text" name="username" id="username" class="form-control" placeholder="Username" autofocus style="border-bottom-right-radius: 0; border-bottom-left-radius: 0; border-bottom:0;">
        <input type="password" name="password" id="password" class="form-control" placeholder="Password">
        <label class="checkbox hidden-xs" style="line-height:1">
            <input type="checkbox" name="remember" value="1"> &nbsp; &nbsp; &nbsp; Remember me for one week
        </label>
        <button class="btn btn-lg btn-success btn-block" id="submit-btn" type="submit">Sign in</button>
        <p class="text-center"><br><a href="#" class="gray" onclick="return showPasswordForm()"><i class="fa fa-key"></i> Reset password?</a></p>
    </form>

    <form class="form-signin" action="./home.php" method="post" onsubmit="return doPassword();" id="password-form" style="display: none;">
        <h2 class="form-signin-heading">Reset Password</h2>
        <p id="password-msg" class="alert" style="display:none"></p>
        <input type="text" name="username" id="pusername" class="form-control" placeholder="Username" autofocus style="border-radius:4px; margin-bottom:10px;">
        <button class="btn btn-lg btn-success btn-block" id="submit-btn" type="submit">Reset Password</button>
        <p class="text-center"><br><a href="#" class="gray" onclick="return showLoginForm()"><i class="fa fa-reply"></i> &nbsp;back to login form</a></p>
    </form>

</div></div>

<div id="footer"><div class="container">
    <p class="pull-left text-muted credit">
        &copy; 2014 RevenueHits. All rights reserved to MyAdWise Ltd.</span><br>
        <a href="./terms"><span class="hidden-xs">Advertising</span> Guidelines</a> &nbsp;
        <a href="./terms">Terms<span class="hidden-xs"> of Service</span></a> &nbsp;
        <a href="./privacy">Privacy<span class="hidden-xs"> Policy</span></a> &nbsp;
        <a href="./contact">Support</a>
    </div>
    <div class="clearfix"></div>
</div></div>

</body>
</html>