<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <meta name="robots" content="noindex">
    <link rel="shortcut icon" href="./favicon.ico">

    <title>Admin Console - Dashboard</title>

    <!-- Bootstrap + Custom CSS -->
    <link href="./media/css/bootstrap.css" rel="stylesheet">
    <link href="./media/css/bootstrap-theme.css" rel="stylesheet">
    <link href="./media/css/font-awesome.css" rel="stylesheet">
    <link href="./media/css/font-proxima.css" rel="stylesheet">
    <link href="./media/css/icons.css" rel="stylesheet">
    <link href="./media/css/layout.css" rel="stylesheet">
    <link href="./media/css/animation.css" rel="stylesheet">

    <!-- Bootstrap + jQuery + Custom JS -->
    <script src="./media/js/jquery.min.js" ></script>
    <script src="./media/js/bootstrap.min.js"></script>

    <!--  date picker -->
    <link href="./media/css/daterangepicker.css" rel="stylesheet">
    <script src="./media/js/moment.js"></script>
    <script src="./media/js/daterangepicker.js" ></script>

    <!-- chart stuff -->
    <link rel="stylesheet" href="./media/css/morris.css" rel="stylesheet">
    <script src="./media/js/raphael-min.js"></script>
    <script src="./media/js/morris.min.js"></script>

    <!-- touchspin -->
    <link rel="stylesheet" href="./media/css/touchspin.min.css" rel="stylesheet">
    <script src="./media/js/touchspin.min.js"></script>

    <!-- advanced select -->
    <link rel="stylesheet" href="./media/css/bootstrap-select.min.css" rel="stylesheet">
    <script src="./media/js/bootstrap-select.min.js"></script>

    <!-- SweetAlert - alert replacer -->
    <link href="./media/css/sweet-alert.css" rel="stylesheet">
    <script src="./media/js/sweet-alert.min.js" ></script>

    <!-- NProgress - Progress Bar -->
    <link href="./media/css/nprogress.css" rel="stylesheet">
    <script src="./media/js/nprogress.js" ></script>

    <!-- main Javascript -->
    <script src="./media/js/common.js"></script>
    <script src="./media/js/browsercheck.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="./media/js/html5shiv.min.js"></script>
        <script src="./media/js/respond.min.js"></script>
    <![endif]-->
</head>

<body class="preload <?php echo @$bodyclass; ?>">
<script> NProgress.start(); </script>

<!-- if there's a problem, put the message here and give the body a "notice" class -->
<div id="notice">This is a global message that tells the advertiser that something is wrong. Usually this would alert the advertiser of a low balance.</div>

<!-- start page -->
<div id="wrap">

<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><i class="fa fa-lg fa-bars"></i></button>
            <a class="navbar-brand" href="./home.php"></a>
        </div>
        <div class="navbar-collapse collapse">
            <?php if (@$_GET['first'] != 1): ?>
            <ul class="nav navbar-nav navbar-main">
                <li<?php if (strpos(@$bodyclass, 'campaign') !== FALSE) echo ' class="active"'; ?>><a href="./home.php">Campaigns</a></li>
                <li<?php if (strpos(@$bodyclass, 'account') !== FALSE) echo ' class="active"'; ?>><a href="./account.php">Account Settings</a></li>
                <li class="<?php if (strpos($bodyclass, 'support') !== FALSE) echo "active "; ?>hidden-xs"><a href="./support.php">Support</a></li>
                <li class="visible-xs"><a href="./billing.php">Billing</a></li>
                <li class="visible-xs"><a href="./logout.php">Logout</a></li>
            </ul>
            <div class="navbar-user pull-right hidden-xs">
                <ul class="nav navbar-nav">
                    <li class="light user-billing">
                        <a href="./billing.php">
                            <div class="pull-left text-right text-sm hidden-sm">
                                <div class="topbalance">Remaining Balance: <span><small>$</small>728.12</span></div>
                                user-name-goes-here
                            </div>
                        </a>
                    </li>
                    <li class="light user-account">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-gear"></i> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right arrow-right">
                            <li><a href="./account.php">Account</a></li>
                            <li><a href="./billing.php">Billing</a></li>
                            <li><a href="./support.php">Support</a></li>
                            <li class="divider"></li>
                            <li><a href="./logout.php">Logout</a></li>
                        </ul>
                    </li>

                    <?php if (strpos(@$bodyclass, 'notice') === FALSE): ?>
                    <!-- if everything's ok - show this: -->
                    <li class="light user-messages"><a name="#" class="tooltip-auto" title="Your account status is in good condition"><i class="fa fa-bell"></i><i class="fa fa-check-circle"></i></a></li>
                    <?php else: ?>
                    <!-- if there's a problem - show this: -->
                    <li class="light user-messages"><a href="#" onclick="$('#notice').toggle(); $(body).toggleClass('notice');"><i class="fa fa-bell"></i><i class="fa fa-exclamation-circle"></i></a></li>
                    <?php endif; ?>
                </ul>
            </div>
            <?php endif; ?>
        </div><!--/.navbar-collapse -->
    </div>
</div>