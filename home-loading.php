<?php
// header
$bodyclass = 'notice campaign-group';
require_once('_header.php');

// sidebar
require_once('_sidebar.php');

// top tabs
$group_view = true;
$active_tab = 'all';
require_once('_tabs.php');
?>

<div class="container" id="maincontent">

    <!-- main chart -->
    <div id="mobile-daterage"></div>
    <div class="chart-wrapper">
        <div class="chart-legend">
            <i class="fa fa-square" style="color:#009966"></i> Views &nbsp;
            <i class="fa fa-square" style="color:#ef7e00"></i> Cost
        </div>
        <div style="overflow:hidden;">
            <div style="height:240px; text-align:center"><i style="font-size:128px;line-height:210px;color:#c6ecdf" class="fa fa-spinner animate-spin"></i></div>
        </div>
    </div>

    <!-- toolbar -->
    <div class="clearfix"></div>
    <div id="toolbar">
        <div class="dropdown main-button inline">
            <a href="#" class="dropdown-toggle btn btn-green" data-toggle="dropdown"><i class="fa fa-plus"></i> New Campaign <i class="fa fa-caret-down"></i></a>
            <ul class="dropdown-menu arrow-left">
                <li><a href="create-campaign.php"><i class="fa fa-eye"></i>&nbsp; Pay Per View (PPV)</a></li>
                <li><a href="create-campaign.php?type=ppc"><i class="fa fa-hand-o-up"></i>&nbsp; Pay Per Click (PPC)</a></li>
            </ul>
        </div>
        <a href="#" class="btn btn-silver"><i class="fa fa-arrow-circle-down" style="font-size:1em;margin-left:-3px;"></i> &nbsp;Download&nbsp;</a>
    </div>

    <!-- main table -->
    <table class="table table-bordered table-hover" id="listitems">
        <thead>
            <tr class="tophead">
                <th colspan="4" class="rightdiv"></th>
                <th colspan="3" class="rightdiv">PPV</th>
                <th colspan="5">PPC</th>
            </tr>
        </thead>
        <thead>
            <tr class="active">
                <th class="text-left">Group</th>
                <th class="text-left">Campaigns</th>
                <th>Total Budget</th>
                <th class="rightdiv">Total Cost</th>

                <th>Impr.</th>
                <th>Avg. CPV</th>
                <th class="rightdiv">Cost</th>

                <th>Impr.</th>
                <th>Clicks</th>
                <th>Avg. CTR</th>
                <th>Avg. CPC</th>
                <th>Cost</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-left"><a href="campaign-group.php" class="settings">Campaign Group</a></td>
                <td class="text-left"><a href="campaign-group.php" class="green">2 running</a>, <a href="campaign-group.php" class="orange">1 pending approval</a></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
                <td class="rightdiv"><i class="fa fa-spinner animate-spin"></i></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
                <td class="rightdiv"><i class="fa fa-spinner animate-spin"></i></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
            </tr>
            <tr>
                <td class="text-left"><a href="campaign-group.php" class="settings">Campaign Group 2</a></td>
                <td class="text-left"><a href="campaign-group.php" class="red">3 out of budget campaigns (1 pending)</a></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
                <td class="rightdiv"><i class="fa fa-spinner animate-spin"></i></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
                <td class="rightdiv"><i class="fa fa-spinner animate-spin"></i></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
                <td><i class="fa fa-spinner animate-spin"></i></td>
            </tr>
        </tbody>
    </table>
    <p class="light-text"><strong style="color:#c00">*</strong> Clicks, CTR and CPC are calculated for PPC-based campaigns. CPV is calculated for PPV-based campaigns.</p>

<?php
require_once('_pagination.php');
require_once('_footer.php');
?>