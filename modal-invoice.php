<!-- Invoice Modal -->
<div class="modal fade" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="invoiceModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sms">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="invoiceModalLabel">Invoicee not provided!</h4>
            </div>
            <div class="modal-body">
                <p>Before your first invoice download, we need to know the <strong>invoicee name</strong> you want us to issue the invoice for.</p>
                <p>You can choose to either use your name, or <a href="./account.php?invoicee=1">enter an invoicee name</a> (you can update the invoicee name at any time under the account tab).</p>
            </div>
            <hr class="no-margin-top">
            <p class="text-center">
                <a class="btn btn-silver" href="#">Use my name as the Invoicee name</a>
                &nbsp;
                <a class="btn btn-green" href="./account.php?invoicee=1">I want to update my Invoicee name first</a>
            </p>
            &nbsp;
        </div>
    </div>
</div>
