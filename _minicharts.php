    <div id="mini-charts">
        <div class="chart-box"><div class="mini-chart">
            <div class="text-center"><i class="icon fa fa-signal" style="zoom:1.25;margin:0;right:0;position:relative;"></i></div>
            <h1>120,690</h1>
            <h2>TOTAL TRAFFIC</h2>
            <p>* Total number of visits (including Clicks, CPVs, Redirects, etc) to your site</p>
        </div></div>
        <div class="chart-box"><div class="mini-chart">
            <i class="icon fa fa-random"></i>
            CAMPAIGN TYPES
            <table class="piechart"><tr>
            <td><div class="morris-mini-chart" id="minichart-campaign-type"></div></td>
            <td><ul class="list-unstyled legend">
                <li><i class="fa fa-square" style="color:#0c7930"></i> Pop</li>
                <li><i class="fa fa-square" style="color:#9cdbaf"></i> Redirect</li>
                <li><i class="fa fa-square" style="color:#4ea66a"></i> Search</li>
                <li><i class="fa fa-square" style="color:#1c833d"></i> Native Ads</li>
                <li><i class="fa fa-square" style="color:#83ca99"></i> ...</li>
            </ul></td>
            </tr></table>
        </div></div>
        <div class="chart-box"><div class="mini-chart">
            <i class="icon fa fa-laptop"></i>
            TRAFFIC BY DEVICE
            <table class="barchart">
                <tr><th>Mobile </th><td><div data-toggle="tooltip" class="bar-wrapper"><div class="bar" rel-data="36"></div></div></td></tr>
                <tr><th>Tablet </th><td><div data-toggle="tooltip" class="bar-wrapper"><div class="bar" rel-data="13"></div></div></td></tr>
                <tr><th>Desktop</th><td><div data-toggle="tooltip" class="bar-wrapper"><div class="bar" rel-data="51"></div></div></td></tr>
            </table>
        </div></div>
        <div class="chart-box"><div class="mini-chart">
            <i class="icon fa fa-flag-o"></i>
            TRAFFIC &amp; CONVERSIONS
            <div class="morris-mini-chart" id="minichart-traffic-conversions"></div>
        </div></div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    <hr>

    <script>
    // mini charts data
    ctData = [
        {label: "Pop", value: 40},
        {label: "Domains", value: 20},
        {label: "Search", value: 30},
        {label: "Native Ads", value: 10}
    ];
    tcData = [
        { "period": "2014-10-18", "traffic": 12345, "conversions": (123*250) },
        { "period": "2014-10-19", "traffic": 0, "conversions": 0 },
        { "period": "2014-10-20", "traffic": 12345, "conversions": (123*250) },
        { "period": "2014-10-21", "traffic": 18524, "conversions": (14*250) },
        { "period": "2014-10-22", "traffic": 12345, "conversions": (123*250) },
        { "period": "2014-10-23", "traffic": 19384, "conversions": (193*250) },
        { "period": "2014-10-24", "traffic": 12345, "conversions": (123*250) },
        { "period": "2014-10-25", "traffic": 12266, "conversions": (122*250) }
    ];
    </script>